﻿using System.Data.Entity;
using CarService_WebApp.Models;
using CarService_WebApp.Models.Auth;
using System.Data.Entity.ModelConfiguration.Conventions;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CarService_WebApp.Db
{
    public class EFDbContext : IdentityDbContext<User, Role, int, UserLogin, UserRole, UserClaim>
    {
        public EFDbContext() : base("AuthEFDbContext") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            //modelBuilder.Entity<IdentityUser>().ToTable("Users").Property(p => p.Id).HasColumnName("UserID");
            //modelBuilder.Entity<IdentityRole>().ToTable("Roles").Property(p => p.Id).HasColumnName("RoleID");
            //modelBuilder.Entity<IdentityUserLogin>().HasKey<string>(l => l.UserId);
            //modelBuilder.Entity<IdentityUserRole>().HasKey(r => new { r.RoleId, r.UserId });
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Mechanic> Mechanics { get; set; }
        public DbSet<ClientXCar> ClientsXCars { get; set; }
        public DbSet<RepairXPart> RepairsXParts { get; set; }
        public DbSet<Part> Parts { get; set; }
        public DbSet<Repair> Repairs { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<IncomingRepair> IncomingRepairs { get; set; }
    }
}