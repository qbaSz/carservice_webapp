namespace CarService_WebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsActiveColumnToCarsTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cars", "IsActive", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cars", "IsActive");
        }
    }
}
