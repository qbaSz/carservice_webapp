namespace CarService_WebApp.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CarService_WebApp.Db.EFDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "CarService_WebApp.Db.EFDbContext";
        }

        protected override void Seed(CarService_WebApp.Db.EFDbContext context)
        {

        }
    }
}
