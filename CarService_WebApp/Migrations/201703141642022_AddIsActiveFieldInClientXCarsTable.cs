namespace CarService_WebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsActiveFieldInClientXCarsTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ClientXCars", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ClientXCars", "IsActive");
        }
    }
}
