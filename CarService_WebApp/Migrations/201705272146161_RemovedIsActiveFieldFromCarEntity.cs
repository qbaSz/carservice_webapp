namespace CarService_WebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedIsActiveFieldFromCarEntity : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Cars", "IsActive");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Cars", "IsActive", c => c.Boolean(nullable: false));
        }
    }
}
