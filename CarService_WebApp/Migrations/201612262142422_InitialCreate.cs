namespace CarService_WebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cars",
                c => new
                    {
                        CarID = c.Int(nullable: false, identity: true),
                        Model = c.String(nullable: false),
                        Make = c.String(nullable: false),
                        Year = c.Int(nullable: false),
                        Capacity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Bhp = c.Int(nullable: false),
                        VinNo = c.String(),
                    })
                .PrimaryKey(t => t.CarID);
            
            CreateTable(
                "dbo.ClientXCars",
                c => new
                    {
                        ClientXCarID = c.Int(nullable: false, identity: true),
                        PlateNo = c.String(nullable: false),
                        CarID = c.Int(nullable: false),
                        ClientID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ClientXCarID)
                .ForeignKey("dbo.Cars", t => t.CarID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .Index(t => t.CarID)
                .Index(t => t.ClientID);
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        ClientID = c.Int(nullable: false, identity: true),
                        UserID = c.Int(nullable: false),
                        HasDiscount = c.Boolean(nullable: false),
                        Discount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ClientID)
                .ForeignKey("dbo.AspNetUsers", t => t.UserID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.Repairs",
                c => new
                    {
                        RepairID = c.Int(nullable: false, identity: true),
                        RepairHash = c.String(),
                        StartDate = c.DateTime(precision: 7, storeType: "datetime2"),
                        RequestDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        PlannedEndDate = c.DateTime(precision: 7, storeType: "datetime2"),
                        IsFinished = c.Boolean(nullable: false),
                        Description = c.String(),
                        MechanicID = c.Int(nullable: false),
                        ClientID = c.Int(nullable: false),
                        ClientXCarID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RepairID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .ForeignKey("dbo.ClientXCars", t => t.ClientXCarID)
                .ForeignKey("dbo.Mechanics", t => t.MechanicID)
                .Index(t => t.MechanicID)
                .Index(t => t.ClientID)
                .Index(t => t.ClientXCarID);
            
            CreateTable(
                "dbo.Mechanics",
                c => new
                    {
                        MechanicID = c.Int(nullable: false, identity: true),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MechanicID)
                .ForeignKey("dbo.AspNetUsers", t => t.UserID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoleID = c.Int(nullable: false),
                        Name = c.String(),
                        Surname = c.String(),
                        Email = c.String(maxLength: 256),
                        Phone = c.Int(nullable: false),
                        Adress = c.String(),
                        City = c.String(),
                        IsAuthenticated = c.Boolean(nullable: false),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleID)
                .Index(t => t.RoleID)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.RepairXParts",
                c => new
                    {
                        RepairXPartID = c.Int(nullable: false, identity: true),
                        RepairID = c.Int(nullable: false),
                        PartID = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Total = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.RepairXPartID)
                .ForeignKey("dbo.Parts", t => t.PartID)
                .ForeignKey("dbo.Repairs", t => t.RepairID)
                .Index(t => t.RepairID)
                .Index(t => t.PartID);
            
            CreateTable(
                "dbo.Parts",
                c => new
                    {
                        PartID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.PartID);
            
            CreateTable(
                "dbo.Currencies",
                c => new
                    {
                        CurrencyID = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                    })
                .PrimaryKey(t => t.CurrencyID);
            
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        InvoiceID = c.Int(nullable: false, identity: true),
                        RepairID = c.Int(nullable: false),
                        Number = c.String(),
                        IssueDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DueDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        GrossAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsPaid = c.Boolean(nullable: false),
                        NetAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        VatAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CurrencyID = c.Int(nullable: false),
                        PaymentDay = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.InvoiceID)
                .ForeignKey("dbo.Currencies", t => t.CurrencyID)
                .ForeignKey("dbo.Repairs", t => t.RepairID)
                .Index(t => t.RepairID)
                .Index(t => t.CurrencyID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Invoices", "RepairID", "dbo.Repairs");
            DropForeignKey("dbo.Invoices", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.Clients", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.RepairXParts", "RepairID", "dbo.Repairs");
            DropForeignKey("dbo.RepairXParts", "PartID", "dbo.Parts");
            DropForeignKey("dbo.Mechanics", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "RoleID", "dbo.AspNetRoles");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Repairs", "MechanicID", "dbo.Mechanics");
            DropForeignKey("dbo.Repairs", "ClientXCarID", "dbo.ClientXCars");
            DropForeignKey("dbo.Repairs", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.ClientXCars", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.ClientXCars", "CarID", "dbo.Cars");
            DropIndex("dbo.Invoices", new[] { "CurrencyID" });
            DropIndex("dbo.Invoices", new[] { "RepairID" });
            DropIndex("dbo.RepairXParts", new[] { "PartID" });
            DropIndex("dbo.RepairXParts", new[] { "RepairID" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUsers", new[] { "RoleID" });
            DropIndex("dbo.Mechanics", new[] { "UserID" });
            DropIndex("dbo.Repairs", new[] { "ClientXCarID" });
            DropIndex("dbo.Repairs", new[] { "ClientID" });
            DropIndex("dbo.Repairs", new[] { "MechanicID" });
            DropIndex("dbo.Clients", new[] { "UserID" });
            DropIndex("dbo.ClientXCars", new[] { "ClientID" });
            DropIndex("dbo.ClientXCars", new[] { "CarID" });
            DropTable("dbo.Invoices");
            DropTable("dbo.Currencies");
            DropTable("dbo.Parts");
            DropTable("dbo.RepairXParts");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Mechanics");
            DropTable("dbo.Repairs");
            DropTable("dbo.Clients");
            DropTable("dbo.ClientXCars");
            DropTable("dbo.Cars");
        }
    }
}
