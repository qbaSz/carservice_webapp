namespace CarService_WebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IncomingRepairsForTheAPIpurposeTableAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.IncomingRepairs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Status = c.Byte(nullable: false),
                        ClientId = c.Int(nullable: false),
                        MechanicId = c.Int(),
                        ClientXCarId = c.Int(),
                        IncomingRepairDateTime = c.DateTime(nullable: false),
                        MessageFromClient = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.ClientId)
                .ForeignKey("dbo.ClientXCars", t => t.ClientXCarId)
                .ForeignKey("dbo.Mechanics", t => t.MechanicId)
                .Index(t => t.ClientId)
                .Index(t => t.MechanicId)
                .Index(t => t.ClientXCarId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.IncomingRepairs", "MechanicId", "dbo.Mechanics");
            DropForeignKey("dbo.IncomingRepairs", "ClientXCarId", "dbo.ClientXCars");
            DropForeignKey("dbo.IncomingRepairs", "ClientId", "dbo.Clients");
            DropIndex("dbo.IncomingRepairs", new[] { "ClientXCarId" });
            DropIndex("dbo.IncomingRepairs", new[] { "MechanicId" });
            DropIndex("dbo.IncomingRepairs", new[] { "ClientId" });
            DropTable("dbo.IncomingRepairs");
        }
    }
}
