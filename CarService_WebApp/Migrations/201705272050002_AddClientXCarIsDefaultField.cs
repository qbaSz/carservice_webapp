namespace CarService_WebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddClientXCarIsDefaultField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ClientXCars", "IsDefault", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ClientXCars", "IsDefault");
        }
    }
}
