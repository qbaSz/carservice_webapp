// <auto-generated />
namespace CarService_WebApp.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class DeleteIsActiveColumnCarsTable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(DeleteIsActiveColumnCarsTable));
        
        string IMigrationMetadata.Id
        {
            get { return "201703121539502_DeleteIsActiveColumnCarsTable"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
