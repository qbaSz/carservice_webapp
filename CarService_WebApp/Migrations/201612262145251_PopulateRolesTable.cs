using System.Collections.Generic;
using System.Linq;

namespace CarService_WebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateRolesTable : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT AspNetRoles ON");

            for (var i = 0; i < RolesEnumerator.Roles().Count(); i++)
            {
                var id = i + 1;
                var name = RolesEnumerator.Roles().ElementAt(i);

                Sql("BEGIN " +
                    "IF NOT EXISTS " +
                    "(SELECT * FROM AspNetRoles WHERE Id = '" + id + "' AND Name = '" + name + "' ) " +
                    "BEGIN" +
                    " INSERT INTO AspNetRoles (Id, Name) " +
                    " VALUES ('" + id +  "','" + name + "') " +
                    " END " +
                    "END");
            }

            Sql("SET IDENTITY_INSERT AspNetRoles OFF");
        }
        
        public override void Down()
        {
        }
    }

    public class RolesEnumerator
    {
        public static IEnumerable<string> Roles()
        { 
            return new List<string>
                {
                    "Client",
                    "Mechanic",
                    "Admin",
                    "GuestClient",
                };
        }
    }
}
