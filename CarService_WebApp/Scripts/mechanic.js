﻿///<reference path="~/Scripts/jquery-3.1.1.intellisense.js">
///<reference path="~/Scripts/jquery-3.1.1.js">
///<reference path="~/Scripts/jquery.unobtrusive-ajax.js">

$(document).ready(function () {
});

$(document).on("click", ".change-week", function () {
    var date = $(this).attr("id");
    $.ajax({
        url: "/Mechanic/RepairsForCurrentWeek",
        method: "POST",
        data: { startDate: date }        
    }).done(function (response) {
        $("#week-repairs-table").html(response);
    })
})

$(document).on("click", ".current-week", function () {
    $.ajax({
        url: "/Mechanic/RepairsForCurrentWeek",
        method: "POST",
        data: { startDate: null }
    }).done(function (response) {
        $("#week-repairs-table").html(response);
    })
})

$(document).on("mouseenter", ".clients-list-row", function () {
    $(this).addClass("clients-list-row-hover");
});

$(document).on("mouseleave", ".clients-list-row", function () {
    $(this).removeClass("clients-list-row-hover");
});

$(document).on("click", ".clients-list-row", function () {
    $(".clients-list-row-selected").removeClass("clients-list-row-selected");
    $(this).addClass("clients-list-row-selected");
    $("#FormUser_Id").val($(this).attr("id"));
    $("#FormUser_Name").val($.trim($(this).find("#name").text()));
    $("#FormUser_Surname").val($.trim($(this).find("#surname").text()));
    $("#FormUser_Phone").val($.trim($(this).find("#phone").text()));
});

//Searching clients by surname on Create Repair page
//----------------------------------------------------------
$(document).on("click", "#btn-search-clients", function () {
    var surname = $("#surname").val();
    $.ajax({
        url: "Find",
        method: "POST",
        data: { surname: surname },
        beforeSend: BeginClientListInsertion()
    }).done(function (response) {
        $(".loading-bar").hide();
        $("#listOfUsers").html(response);
    });
});

function InsertClientList(response) {
    $(".loading-bar").hide();
    $("#listOfUsers").html(response);
}

function BeginClientListInsertion() {
    $(".loading-bar").show();
}
//-----------------------------------------------------------

//Add part to the list
//----------------------------------------------------
$(document).on("click", "#btn-add-part", function () {
    var partName = $("#Part_Name").val();
    var partPrice = $("#Part_Price").val();
    var partQuantity = $("#Quantity").val();
    $.ajax({
        url: "AddPartToList",
        method: "POST",
        data: { partName: partName, partPrice: partPrice, quantity: partQuantity },
    }).done(function (response) {
        $("#Part_Name").val("");
        $("#Part_Price").val("");
        $("#Quantity").val("");
        AppendPartToList(response);
    });
});

function AppendPartToList(response) {
    $(".part-list-table").append(response);
}
//-----------------------------------------------------

//Remove part from the list
//-------------------------------------------------------
$(document).on("click", ".btn-delete-part", function () {
    var repairXPartId = $(this).attr("id");
    $.ajax({
        url: "DeletePartFromList",
        method: "POST",
        data: { repairXPartId : repairXPartId }
    }).done(function (response) {
        $("tr.part-row#" + response).remove();
    })
});
//--------------------------------------------------------

//Repair list script
//
$(document).on("click", ".btn-delete-repair", function () {
    var repairId = $(this).attr("id");
    $('<div></div>').appendTo('body')
    .html('<div><h6>Are you sure you want to delete the repair? It is irreversible.</h6></div>')
    .dialog({
        modal: true,
        title: 'Delete repair',
        zIndex: 10000,
        autoOpen: true,
        width: 300,
        resizable: false,
        buttons: {
            Yes: function () {
                $.ajax({
                    url: "DeleteRepair",
                    method: "POST",
                    data: { repairId : repairId }
                }).done(function (response) {
                    console.log(response);
                    window.location = response;
                })
                $(this).dialog("close");
            },
            No: function () {
                $(this).dialog("close");
            }
        },
        close: function (event, ui) {
            $(this).remove();
        }
    });
});


$(document).on("click", "#btn-search-repair", function () {
    var plateNo = $("#plateNo").val();
    var surname = $("#clientSurname").val();
    var make = $("#carMake").val();
    var mode = $(".repairs-tabs li.active").attr("id");
    console.log(plateNo);
    $.ajax({
        method: "POST",
        url: "ListFindPlateNo",
        data: { plateNo: plateNo, surname: surname, carMake: make, mode: mode }
    }).done(function (response) {
        $(".repair-list-table").html(response);
    });
});

$(document).on("change", "#FormRepair_StartDate", function () {
    var date = $(this).val();
    console.log("cyce");
    $.ajax({
        method: "POST",
        url: "RepairsForDate",
        data: { startDate: date }
    }).done(function (response) {
        $(".repairs-for-date").html(response);
    })
});

$(document).on("click", ".repairs-tabs li", function () {
    if (!$(this).hasClass("active")) {
        var mode = $(this).attr("id");
        if (mode == "Incoming") {
            $("#plateNo").hide();
            $("#clientSurname").hide();
            $("#carMake").hide();
            $("#btn-search-repair").hide();
        }
        else {
            $("#plateNo").show();
            $("#clientSurname").show();
            $("#carMake").show();
            $("#btn-search-repair").show();
        }
        $(".repairs-tabs").find(".active").removeClass("active");
        $(this).addClass("active");
        var plateNo = $("#plateNo").val();
        var surname = $("#clientSurname").val();
        var make = $("#carMake").val();
        $.ajax({
            method: "POST",
            url: "ListFindPlateNo",
            data: { plateNo: plateNo, surname: surname, carMake: make, mode: mode }
        }).done(function (response) {
            $(".repair-list-table").html(response);
        });
    }
})

$(document).on("click", ".dropdown-make", function () {
    $("#car-model-list").hide();
    $("#car-version-list").hide();
    $("#car-make-list").slideToggle();
})

$(document).on("click", ".dropdown-model", function () {
    $("#car-make-list").hide();
    $("#car-version-list").hide();
    $("#car-model-list").slideToggle();
})

$(document).on("click", ".dropdown-version", function () {
    $("#car-make-list").hide();
    $("#car-model-list").hide();
    $("#car-version-list").slideToggle();
})

$(document).on("click", ".car-dropdown-li", function () {
    var parent = $(this).parent().parent();
    var data = $.trim($(this).text());
    console.log(data);
    $(".car-dropdown").slideUp();
    if (parent.attr("id") == "car-make-list") {
        $(".dropdown-model").show();
        $("#FormCar_Model").val('');
        $("#FormCar_Version").val('');
        $("#FormCar_Make").val(data);
        $("#FormCar_Make").text(data);
        $("#FormCar_Make").attr("value", data);
        $.ajax({
            url: "CarModelList",
            method: "POST",
            data: {make : data}
        }).done(function (response) {
            $("#car-model-list").html(response);
        })
    } else if (parent.attr("id") == "car-model-list") {
        var make = $("#FormCar_Make").val();
        $(".dropdown-version").show();
        $("#FormCar_Model").val(data);
        $("#FormCar_Model").attr("value", data);
        $.ajax({
            url: "CarVersionList",
            method: "POST",
            data: { make: make, model: data }
        }).done(function (response) {
            $("#car-version-list").html(response);
        })
    } else if (parent.attr("id") == "car-version-list") {
        $("#FormCar_Version").val(data);
    }
})

$(document).on("click", "#plate-search", function () {
    var plateNo = $("#FormClientXCar_PlateNo").val();
    console.log("clicked");
    console.log(plateNo);
    if (plateNo != '') {
        $.ajax({
            method: "POST",
            url: "FindPlateNo",
            data: { plateNo: plateNo }
        }).done(function (response) {
            if (response.Success == 1) {
                $("#FormCar_Model").val(response.CarModel);
                $("#FormCar_Make").val(response.CarMake);
                $("#FormCar_Version").val(response.CarVersion);
                $("#FormCar_Capacity").val(response.CarCapacity);
                $("#FormCar_Bhp").val(response.CarBhp);
                $("#FormCar_Year").val(response.CarYear);
                $("#FormClientXCar_PlateNo").val(response.PlateNo);
                $("#FormClientXCar_ClientXCarID").val(response.Id);
                $("#FormUser_Id").val(response.UserId);
                $("#FormUser_Name").val(response.UserName);
                $("#FormUser_Surname").val(response.UserSurname);
                $("#FormUser_Phone").val(response.UserPhone);
                $("#listOfUsers").html('');
            }
            else {
                alert("No car for a given plate number.");
                $("#FormClientXCar_ClientXCarID").val(0);
            }
        })
    }
})
