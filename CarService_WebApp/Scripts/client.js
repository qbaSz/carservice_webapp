﻿///<reference path="~/Scripts/jquery-3.1.1.intellisense.js">
///<reference path="~/Scripts/jquery-3.1.1.js">
///<reference path="~/Scripts/jquery.unobtrusive-ajax.js">

$(document).on("click", ".repairs-tabs li", function () {
    if (!$(this).hasClass("active")) {
        $(".repairs-tabs li.active").removeClass("active");
        $(this).addClass("active");
    }
    var mode = $(this).attr("id");
    if (mode == "Paid" || mode == "NotPaid") {
        $.ajax({
            method: "POST",
            url: "Client/InvoiceList",
            data: { mode: mode }
        }).done(function (response) {
            $(".list-table").html(response);
        })
    }
    else if(mode == "All" || mode == "Finished" || mode == "NotFinished"){
        $.ajax({
            method: "POST",
            url: "Client/ListOfRepairs",
            data: { mode: mode }
        }).done(function (response) {
            $(".list-table").html(response);
        })
    }
})

$(document).on("click", ".drop-parts", function () {
    console.log("#" + $(this).parent().attr("id") + ".mini-parts-list");
    $("#" + $(this).parent().attr("id") + ".mini-parts-list").slideToggle();
})

$(document).on("click", "#plate-search", function () {
    var plateNo = $("#ClientXCarPlateNo").val();
    console.log("clicked");
    console.log(plateNo);
    if (plateNo != '') {
        $.ajax({
            method: "POST",
            url: "FindPlateNo",
            data: { plateNo: plateNo }
        }).done(function (response) {
            if (response.Success == 1) {
                $("#plate-result").html("Using > <b> " + response.PlateNo + "</b> - " + response.Car).show();
                $("#ClientXCar_PlateNo").val(response.PlateNo);
                $("#ClientXCarId").val(response.Id);
            }
            else
            {
                $("#plate-result").html("No car for given plate number. If you wish you can <a style=\"cursor: pointer\" id=\"add-car\">add car here</a>").show();
            }
        })
    }
})

$(document).on("click", "#add-car", function () {
    $.ajax({
        method: "POST",
        url: "AddCarView"
    }).done(function (response) {
        $("#add-car-and-plate").html(response);
        $("#add-car-and-plate").slideDown();
    })

})

$(document).on("click", ".dropdown-make", function () {
    $("#car-model-list").hide();
    $("#car-version-list").hide();
    $("#car-make-list").slideToggle();
})

$(document).on("click", ".dropdown-model", function () {
    $("#car-make-list").hide();
    $("#car-version-list").hide();
    $("#car-model-list").slideToggle();
})

$(document).on("click", ".dropdown-version", function () {
    $("#car-make-list").hide();
    $("#car-model-list").hide();
    $("#car-version-list").slideToggle();
})

$(document).on("click", ".car-dropdown-li", function () {
    var parent = $(this).parent().parent();
    var data = $.trim($(this).text());
    console.log(data);
    $(".car-dropdown").slideUp();
    if (parent.attr("id") == "car-make-list") {
        $(".dropdown-model").show();
        $("#Model").val('');
        $("#Version").val('');
        $("#Make").val(data);
        $("#Make").text(data);
        $("#Make").attr("value", data);
        $.ajax({
            url: "CarModelList",
            method: "POST",
            data: { make: data }
        }).done(function (response) {
            $("#car-model-list").html(response);
        })
    } else if (parent.attr("id") == "car-model-list") {
        var make = $("#Make").val();
        $(".dropdown-version").show();
        $("#Model").val(data);
        $("#Model").attr("value", data);
        $.ajax({
            url: "CarVersionList",
            method: "POST",
            data: { make: make, model: data }
        }).done(function (response) {
            $("#car-version-list").html(response);
        })
    }
})

$(document).on("click", "#save-car", function () {
    var make = $("#Make").val();
    var model = $("#Model").val();
    var version = $("#Version").val();
    var year = $("#Year").val();
    var capacity = $("#Capacity").val();
    var bhp = $("#Bhp").val();
    var plateNo = $("#ClientXCarPlateNo").val();
    if (make == '' || model == '' || version == '' || year == '' || capacity == '' || bhp == '' || plateNo == '') {
        $("#blank-car-input").show();
    }
    else
    {
        $("#blank-car-input").hide();
        $("#add-car-and-plate").slideUp();
        $.ajax({
            method: "POST",
            url: "AddCarToDb",
            data: {plateNo: plateNo, make: make, model: model, version: version, year: year, capacity: capacity, bhp: bhp}
        }).done(function (response) {
            if (response.Success == 1) {
                $("#plate-result").html("Using > <b> " + response.PlateNo + "</b> - " + response.Car).show();
                $("#ClientXCar_PlateNo").val(response.PlateNo);
                $("#ClientXCarId").val(response.Id);
            }
            else {
                $("#plate-result").html("Something went wrong. If you wish you can <a style=\"cursor: pointer\" id=\"add-car\">add car here</a>").show();
            }
            $("#add-car-and-plate").html("");
        })
    }
})

$(document).ready(function () {
    $("#request-added-info").delay("5000").fadeOut();
})