﻿///<reference path="~/Scripts/jquery-3.1.1.intellisense.js">
///<reference path="~/Scripts/jquery-3.1.1.js">
///<reference path="~/Scripts/jquery.unobtrusive-ajax.js">

$(document).on("click", ".repairs-tabs li", function () {
    if (!$(this).hasClass("active")) {
        $(".repairs-tabs li.active").removeClass("active");
        $(this).addClass("active");
    }
    var mode = $(this).attr("id");
    $(".btn-admin-search").attr("data-mode", mode);
    $("#AddMechanicBtn").hide();
    if (mode == "PaidInvoices" || mode == "NotPaidInvoices") {
        $("#MechanicSurname").hide();
    } else if (mode == "AllClients"){
        $("#MechanicSurname").hide();
        $("#PlateNo").hide();
        $("#CarMake").hide();
        $("#ClientSurname").show();
    } else if (mode == "AllMechanics") {
        $("#MechanicSurname").show();
        $("#AddMechanicBtn").show();
        $("#PlateNo").hide();
        $("#CarMake").hide();
        $("#ClientSurname").hide();
    } else {
        $("#MechanicSurname").show();
        $("#PlateNo").show();
        $("#CarMake").show();
        $("#ClientSurname").show();
    }
    $.ajax({
        method: "POST",
        url: "Admin/" + mode,
        beforeSend: ShowLoadingBar()
    }).done(function (response) {
        $(".loading-text").hide();
        $(".list-table").html(response);
    })
})

$(document).on("click", ".btn-admin-search", function () {
    var mode = $(this).attr("data-mode");
    var plateNo = $("#PlateNo").val();
    var carMake = $("#CarMake").val();
    var clientSurname = $("#ClientSurname").val();
    var mechanicSurname = $("#MechanicSurname").val();
    var objToSend = {};
    if (mode == "AllRepairs" || mode == "FinishedRepairs" || mode == "NotFinishedRepairs") {
        objToSend = { plateNo: plateNo, carMake: carMake, clientSurname: clientSurname, mechanicSurname: mechanicSurname };
    } else if (mode == "PaidInvoices" || mode == "NotPaidInvoices") {
        objToSend = { plateNo: plateNo, carMake: carMake, clientSurname: clientSurname };
    } else if (mode == "AllClients") {
        objToSend = { clientSurname: clientSurname };
    } else if (mode == "AllMechanics") {
        objToSend = { mechanicSurname: mechanicSurname };
    }
    console.log(objToSend);
    $.ajax({
        method: "POST",
        url: "Admin/" + mode,
        data: objToSend,
        beforeSend: ShowLoadingBar()
    }).done(function (response) {
        $(".loading-text").hide();
        $(".list-table").html(response);
    }) 
})

function ShowLoadingBar() {
    $(".loading-text").show();
}

$(document).on("click", ".drop-parts", function () {
    console.log("#" + $(this).parent().attr("id") + ".mini-parts-list");
    $("#" + $(this).parent().attr("id") + ".mini-parts-list").slideToggle();
})

$(document).on("click", ".mechanic-list-row", function () {
    var mechanicId = $(this).attr("id");
    $.ajax({
        method: "POST",
        url: "Admin/MechanicDetails",
        data: { mechanicId: mechanicId }
    }).done(function (response) {
        $(".list-table").html(response);
    })
})

$(document).on("click", ".repair-list-row", function () {
    window.location = $(this).attr("data-href");
})

function BackToMechanicsList() {
    $("#AllMechanics").click();
}
