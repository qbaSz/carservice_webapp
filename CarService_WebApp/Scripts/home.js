﻿///<reference path="~/Scripts/jquery-3.1.1.intellisense.js">
///<reference path="~/Scripts/jquery-3.1.1.js">
///<reference path="~/Scripts/jquery.unobtrusive-ajax.js">

function ShowRepairStatusDialog(response) {
        $(".repair-status-box").html(response);
        $(".dialog-box").fadeIn();
}

function CloseRepairStatusDialog() {
    $(".dialog-box").fadeOut(function () {
        $(".repair-status-box").html("");
        $("#plateNo").val("");
        $("#repairHash").val("");
    });
}
