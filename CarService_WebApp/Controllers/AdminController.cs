﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarService_WebApp.Db;
using CarService_WebApp.Models;
using CarService_WebApp.Models.Auth;
using Microsoft.AspNet.Identity;
using System.Security.Claims;

namespace CarService_WebApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        public EFDbContext db = new EFDbContext();
        // GET: Admin
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult AllRepairs(string plateNo, string carMake, string clientSurname, string mechanicSurname)
        {
            var repairs = db.Repairs.AsQueryable();
            return FilterRepairs(repairs, plateNo, carMake, clientSurname, mechanicSurname);
        }

        public ActionResult FinishedRepairs(string plateNo, string carMake, string clientSurname, string mechanicSurname)
        {
            var repairs = db.Repairs.Where(r => r.IsFinished);
            return FilterRepairs(repairs, plateNo, carMake, clientSurname, mechanicSurname);
        }

        public ActionResult NotFinishedRepairs(string plateNo, string carMake, string clientSurname, string mechanicSurname)
        {
            var repairs = db.Repairs.Where(r => !r.IsFinished);
            return FilterRepairs(repairs, plateNo, carMake, clientSurname, mechanicSurname);
        }

        public ActionResult PaidInvoices(string plateNo, string carMake, string clientSurname)
        {
            var invoices = db.Invoices.Where(i => i.IsPaid);
            return FilterInvoices(invoices, plateNo, carMake, clientSurname);
        }

        public ActionResult NotPaidInvoices(string plateNo, string carMake, string clientSurname)
        {
            var invoices = db.Invoices.Where(i => !i.IsPaid);
            return FilterInvoices(invoices, plateNo, carMake, clientSurname);
        }

        public ActionResult AllClients(string clientSurname)
        {
            var clients = db.Clients.AsQueryable();
            if(clientSurname == null)
            {
                return View(clients);
            }
            if(clientSurname != "")
            {
                clients = clients.Where(c => c.User.Surname.ToUpper().Equals(clientSurname.ToUpper()));
            }
            return View(clients);
        }

        public ActionResult AllMechanics(string mechanicSurname)
        {
            var mechanics = db.Mechanics.AsQueryable();
            if(mechanicSurname == null)
            {
                return View(mechanics);
            }
            if(mechanicSurname != "")
            {
                mechanics = mechanics.Where(m => m.User.Surname.ToUpper().Equals(mechanicSurname.ToUpper()));
            }
            return View(mechanics);
        }

        [HttpPost]
        public ActionResult MechanicDetails(int mechanicId)
        {
            var mechanic = db.Mechanics.Where(m => m.MechanicID.Equals(mechanicId)).FirstOrDefault();
            return View(mechanic);
        }

        [HttpGet]
        public ActionResult AddMechanic()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddMechanic(ClientRegisterModel mechanicModel)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            UserManager<User, int> userManager = Startup.UserManagerFactory.Invoke();
            var user = new User
            {
                UserName = mechanicModel.Email,
                Name = mechanicModel.Name,
                Surname = mechanicModel.Surname,
                Email = mechanicModel.Email,
                Phone = mechanicModel.Phone,
                Adress = mechanicModel.Adress,
                City = mechanicModel.City,
                RoleID = db.Roles.First(r => r.Name == "Mechanic").Id
            };

            var result = userManager.Create(user, mechanicModel.Password);

            if (result.Succeeded)
            {
                Mechanic mechanic = new Mechanic();
                mechanic.UserID = user.Id;
                db.Mechanics.Add(mechanic);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
            return View();

            
        }

        public ActionResult Statistics()
        {           
             
            return PartialView();
        }

        private ActionResult FilterRepairs(IQueryable<Repair> repairs, string plateNo, string carMake, string clientSurname, string mechanicSurname)
        {
            if (plateNo == null && carMake == null && clientSurname == null && mechanicSurname == null)
            {
                return View("AllRepairs", repairs);
            }
            if (plateNo != "")
            {
                repairs = repairs.Where(r => r.ClientXCar.PlateNo.ToUpper().Equals(plateNo.ToUpper()));
            }
            if (carMake != "")
            {
                repairs = repairs.Where(r => r.ClientXCar.Car.Make.ToUpper().Equals(carMake.ToUpper()));
            }
            if (clientSurname != "")
            {
                repairs = repairs.Where(r => r.Client.User.Surname.ToUpper().Equals(clientSurname.ToUpper()));
            }
            if (mechanicSurname != "")
            {
                repairs = repairs.Where(r => r.Mechanic.User.Surname.ToUpper().Equals(mechanicSurname.ToUpper()));
            }
            return View("AllRepairs", repairs);
        }

        private ActionResult FilterInvoices(IQueryable<Invoice> invoices, string plateNo, string carMake, string clientSurname)
        {
            if (plateNo == null && carMake == null && clientSurname == null)
            {
                return View("InvoiceList", invoices.ToList());
            }
            if (plateNo != "")
            {
                invoices = invoices.Where(i => i.Repair.ClientXCar.PlateNo.ToUpper().Equals(plateNo.ToUpper()));
            }
            if (carMake != "")
            {
                invoices = invoices.Where(i => i.Repair.ClientXCar.Car.Make.ToUpper().Equals(carMake.ToUpper()));
            }
            if (clientSurname != "")
            {
                invoices = invoices.Where(i => i.Repair.Client.User.Surname.ToUpper().Equals(clientSurname.ToUpper()));
            }
            return View("InvoiceList", invoices.ToList());
        }
    }
}