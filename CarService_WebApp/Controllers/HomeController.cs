﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarService_WebApp.Db;
using CarService_WebApp.Models;
using System.ComponentModel.DataAnnotations;
using CarService_WebApp.Models.Auth;
using CarService_WebApp.Models.Home;
using CarService_WebApp.Models.Mech;
using System.Security.Claims;

namespace CarService_WebApp.Controllers
{
    public class HomeController : Controller
    {
        EFDbContext db = new EFDbContext();
        // GET: Home
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult CheckRepairStatus()
        {
            return View("Index");
        }

        public ActionResult CheckRepairStatus(CheckStatusModel checkStatusModel)
        {
            if (!ModelState.IsValid)
            {
                return JavaScript("window.location = \"/Home\"");
            }
            ClientXCar clientXCar = db.ClientsXCars.Where(c => c.PlateNo.ToUpper().Equals(checkStatusModel.PlateNo.ToUpper())).FirstOrDefault();
            if(clientXCar == null)
            {
                ViewBag.CheckError = "There is no given plate number registered";
                return JavaScript("window.location = \"/Home/Index\"");
            }
            Repair repair = db.Repairs.Where(r => r.ClientXCarID.Equals(clientXCar.ClientXCarID)
                                      && r.RepairHash.Equals(checkStatusModel.RepairHash)).FirstOrDefault();
            if(repair == null)
            {
                ViewBag.CheckError = "There is no repair for given values";
                return JavaScript("window.location = \"/Home\"");
            }
            Mechanic mechanic = db.Mechanics.Where(m => m.MechanicID.Equals(repair.MechanicID)).FirstOrDefault();
            User userM = db.Users.Where(u => u.Id.Equals(mechanic.UserID)).FirstOrDefault();
            RepairStatusModel repairStatusModel = new RepairStatusModel
            {
                RepairHash = repair.RepairHash,
                MechanicName = userM.Name + " " + userM.Surname + ", phone: " + userM.Phone,
                StartDate = repair.StartDate,
                EndDate = repair.PlannedEndDate,
                Description = repair.Description,
                Total = db.RepairsXParts.Where(r => r.RepairID.Equals(repair.RepairID)).Sum(r => r.Total),
                IsFinished = repair.IsFinished
            };
            return PartialView(repairStatusModel);
        }

        [Authorize(Roles = "Mechanic, Admin, Client")]
        public ActionResult RedirectToDashboard()
        {
            var identity = (ClaimsIdentity)User.Identity;
            string controllerName = identity.Claims.First(c => c.Type == ClaimTypes.Role).Value;
            return RedirectToAction("Index", controllerName);
        } 
    }
}