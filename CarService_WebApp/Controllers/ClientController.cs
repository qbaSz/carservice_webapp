﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security;
using System.Web.Mvc;
using CarService_WebApp.Models;
using CarService_WebApp.Db;
using Microsoft.AspNet.Identity;
using System.Security.Claims;

namespace CarService_WebApp.Controllers
{
    [Authorize(Roles = "Client")]
    public class ClientController : Controller
    {
        EFDbContext db = new EFDbContext();
        public enum ListMode { All, Finished, NotFinished, Paid, NotPaid };
        // GET: Client
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListOfRepairs(ListMode mode)
        {
            int userId = User.Identity.GetUserId<int>();
            int clientId = db.Clients.Where(c => c.UserID.Equals(userId)).FirstOrDefault().ClientID;
            var repairs = db.Repairs.Where(r => r.ClientID.Equals(clientId)).ToList();
            repairs = FilterRepairsWithMode(repairs, mode);
            return View(repairs);
        }

        public ActionResult InvoiceList(ListMode mode)
        {
            int userId = User.Identity.GetUserId<int>();
            int clientId = db.Clients.Where(c => c.UserID.Equals(userId)).FirstOrDefault().ClientID;
            List<Invoice> invoices = new List<Invoice>();
            var repairs = db.Repairs.Where(r => r.ClientID.Equals(clientId));
            foreach(var repair in repairs)
            {
                var invoice = db.Invoices.Where(i => i.RepairID.Equals(repair.RepairID)).FirstOrDefault();
                invoices.Add(invoice);
            }
            invoices = FilterInvoicesWithMode(invoices, mode);
            return View(invoices);
        }

        private List<Invoice> FilterInvoicesWithMode(List<Invoice> list, ListMode mode)
        {
            switch (mode)
            {
                case ListMode.Paid:
                    list = list.Where(i => i.IsPaid).ToList();
                    break;
                case ListMode.NotPaid:
                    list = list.Where(i => !i.IsPaid).ToList();
                    break;
            }
            return list;
        }

        private List<Repair> FilterRepairsWithMode(List<Repair> list, ListMode mode)
        {
            switch (mode)
            {
                case ListMode.Finished:
                    list = list.Where(r => r.IsFinished).ToList();
                    break;
                case ListMode.NotFinished:
                    list = list.Where(r => !r.IsFinished).ToList();
                    break;
            }
            return list;
        }

        [HttpGet]
        public ActionResult RepairRequest()
        {
            IncomingRepair request = new IncomingRepair();
            int userId = User.Identity.GetUserId<int>();
            request.ClientId = db.Clients.Where(c => c.UserID.Equals(userId)).FirstOrDefault().ClientID;
            request.IncomingRepairDateTime = DateTime.Now;
            request.Status = 0;
            return View(request);
        }

        [HttpPost]
        public ActionResult RepairRequest(IncomingRepair incomingRepair)
        {
            TempData["Message"] = "Request has been successfully sent";
            incomingRepair.ClientXCar = null;
            db.IncomingRepairs.Add(incomingRepair);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult FindPlateNo(string plateNo)
        {
            var clientXCar = db.ClientsXCars.Where(c => c.PlateNo.ToUpper().Equals(plateNo.ToUpper())).FirstOrDefault();
            if(clientXCar != null)
            {
                return Json(new {
                    Success = 1,
                    Id = clientXCar.ClientXCarID,
                    PlateNo = clientXCar.PlateNo.ToUpper(),
                    Car = clientXCar.Car.Make + " " + clientXCar.Car.Model + " " + clientXCar.Car.Capacity + " " + clientXCar.Car.Year
                });
            }
            else
            {
                return Json(new { Success = 0 });
            }
        }

        [HttpPost]
        public ActionResult AddCarView()
        {
            return View();
        }

        public ActionResult AddCarToDb(string plateNo, string make, string model, string version, int year, decimal capacity, int bhp)
        {
            Car car = db.Cars.Where(c => c.Make.ToUpper().Equals(make.ToUpper())
                    && c.Model.ToUpper().Equals(model.ToUpper())
                    && c.Year.Equals(year)
                    && c.Capacity.Equals(capacity)
                    && c.Bhp.Equals(bhp)
                    && c.Version.Equals(version))
                    .FirstOrDefault();

            if(car == null)
            {
                car = new Car();
                car.Make = make;
                car.Model = model;
                car.Version = version;
                car.Capacity = capacity;
                car.Year = year;
                car.Bhp = bhp;
                car = db.Cars.Add(car);
            }

            ClientXCar clientXCar = new ClientXCar() { ClientID = User.Identity.GetUserId<int>(), CarID = car.CarID, PlateNo = plateNo };
            clientXCar = db.ClientsXCars.Add(clientXCar);
            db.SaveChanges();
            if (clientXCar != null)
            {
                return Json(new
                {
                    Success = 1,
                    Id = clientXCar.ClientXCarID,
                    PlateNo = clientXCar.PlateNo.ToUpper(),
                    Car = car.Make + " " + car.Model + " " + car.Capacity + " " + car.Year
                });
            }
            else
            {
                return Json(new { Success = 0 });
            }
        }

        public ActionResult CarMakeList()
        {
            var carMakes = db.Cars.Select(s => s.Make.ToString()).Distinct().ToList();
            return View("DropdownCarDescList", carMakes);
        }

        public ActionResult CarModelList(string make)
        {
            var carModels = db.Cars.Where(c => c.Make.Equals(make)).Select(s => s.Model.ToString()).Distinct().ToList();
            return View("DropdownCarDescList", carModels);
        }

        public ActionResult CarVersionList(string make, string model)
        {
            var carVersions = db.Cars.Where(c => c.Make.Equals(make) && c.Model.Equals(model)).Select(s => s.Version.ToString()).Distinct().ToList();
            return View("DropdownCarDescList", carVersions);
        }
    }
}