﻿using CarService_WebApp.Db;
using CarService_WebApp.Models;
using CarService_WebApp.Models.Auth;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CarService_WebApp.Controllers
{
    public class AuthController : Controller
    {
        // GET: Auth
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home");
        }

        private readonly UserManager<User, int> userManager;
        private EFDbContext db = new EFDbContext();
        public AuthController() : this(Startup.UserManagerFactory.Invoke()) { }

        public AuthController(UserManager<User, int> userManager)
        {
            this.userManager = userManager;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing & userManager != null)
                userManager.Dispose();
            base.Dispose(disposing);
        }

        public UserManager<User, int> GetUserManager()
        {
            return this.userManager;
        }
        // GET: Auth
        [HttpGet]
        public ActionResult LogIn(string returnUrl)
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                return View("AccessDenied");
            }
            var model = new LogInModel() { ReturnUrl = returnUrl };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> LogIn(LogInModel logInModel, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var user = await userManager.FindAsync(logInModel.Email, logInModel.Password);

            if (user != null)
            {
                var identity = await userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
                identity.AddClaim(new Claim(ClaimTypes.GivenName, user.Name));
                identity.AddClaim(new Claim(ClaimTypes.Surname, user.Surname));
                identity.AddClaim(new Claim(ClaimTypes.Role, user.Role.Name));
                Request.GetOwinContext().Authentication.SignIn(identity);
                return Redirect(GetRedirectUrl(user.RoleID, returnUrl));
            }

            ModelState.AddModelError("", "Invalid email or password");
            return View();
        }

        public ActionResult LogOut()
        {
            Request.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        private string GetRedirectUrl(int userRole, string returnUrl = "")
        {
            string controllerName = db.Roles.FirstOrDefault(r => r.Id.Equals(userRole))?.Name;

            if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
            {
                return returnUrl;
            }

            return Url.Action("Index", controllerName);
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(ClientRegisterModel registerModel, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var user = new User {
                UserName = registerModel.Email,
                Name = registerModel.Name,
                Surname = registerModel.Surname,
                Email = registerModel.Email,
                Phone = registerModel.Phone,
                Adress = registerModel.Adress,
                City = registerModel.City,
                RoleID = db.Roles.First(r => r.Name == "Client").Id
            };

            var result = await userManager.CreateAsync(user, registerModel.Password);
            if (result.Succeeded)
            {
                var identity = await userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
                identity.AddClaim(new Claim(ClaimTypes.GivenName, user.Name));
                identity.AddClaim(new Claim(ClaimTypes.Surname, user.Surname));
                identity.AddClaim(new Claim(ClaimTypes.Role, "Client"));
                Request.GetOwinContext().Authentication.SignIn(identity);
                Client client = new Client();
                client.UserID = user.Id;
                db.Clients.Add(client);
                db.SaveChanges();
                return Redirect(GetRedirectUrl(user.RoleID));
            }

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }

            return View();
        }

        [HttpGet]
        public ActionResult ChangePassword(string returnUrl)
        {
            return View(new ChangePasswordModel { ReturnUrl = returnUrl });
        }

        [HttpPost]
        public async Task<ActionResult> ChangePassword(ChangePasswordModel changePasswordModel)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var result = await userManager.ChangePasswordAsync(User.Identity.GetUserId<int>(), changePasswordModel.CurrentPassword, changePasswordModel.NewPassword);
            if (result.Succeeded)
            {
                return Redirect(changePasswordModel.ReturnUrl);
            }

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }

            return View();
        }
    }
}