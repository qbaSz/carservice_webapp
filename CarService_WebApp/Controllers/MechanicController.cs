﻿using System;
using System.Linq;
using System.Web.Mvc;
using CarService_WebApp.Db;
using CarService_WebApp.Models;
using CarService_WebApp.Models.Mech;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Security.Claims;
using System.Web;

namespace CarService_WebApp.Controllers
{
    [Authorize(Roles = "Mechanic, Admin")]
    public class MechanicController : Controller
    {
        EFDbContext db = new EFDbContext();
        public enum RepairListMode { All, Incoming, Finished, NotFinished };

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RepairsForCurrentWeek(DateTime? startDate)
        {
            int userId = User.Identity.GetUserId<int>();
            int mechanicId = db.Mechanics.FirstOrDefault(m => m.UserID.Equals(userId)).MechanicID;
            DateTime today = startDate.HasValue ? startDate.Value : DateTime.Today;
            DateTime weekFormToday = today.AddDays(7);
            var repairsList = db.Repairs.Where(x => x.MechanicID.Equals(mechanicId) 
                                        && x.StartDate >= today && x.StartDate <= weekFormToday).ToList();
            Dictionary<DateTime, List<Repair>> list = new Dictionary<DateTime, List<Repair>>();
            list[today] = new List<Repair>();
            for (int i = 1; i < 8; i++)
            {
                list[today.AddDays(i).Date] = new List<Repair>();
            }
            foreach (var repair in repairsList)
            {
                if(repair.StartDate.HasValue)
                    list[repair.StartDate.Value.Date].Add(repair);
                    list[repair.StartDate.Value.Date] = list[repair.StartDate.Value.Date].OrderBy(r => r.StartDate).ToList();
            }
            return View(list);
        }

        public ActionResult RepairsForDate(DateTime startDate)
        {
            int userId = User.Identity.GetUserId<int>();
            int mechanicId = db.Mechanics.FirstOrDefault(m => m.UserID.Equals(userId)).MechanicID;
            var repairs = db.Repairs.Where(r => r.MechanicID.Equals(mechanicId)).ToList();
            repairs = repairs.Where(r => r.StartDate.Value.Date == startDate.Date).ToList();
            return View(repairs);
        }

        [HttpGet]
        public ActionResult List()
        {
            int userId = db.Users.FirstOrDefault(u => u.UserName.Equals(User.Identity.Name)).Id;
            int mechanicId = db.Mechanics.FirstOrDefault(m => m.UserID.Equals(userId)).MechanicID;
            var repairsList = db.Repairs.Where(x => x.MechanicID.Equals(mechanicId)).OrderBy(x => x.RequestDate);
            return View(repairsList);
        }

        [HttpPost]
        public ActionResult ListFindPlateNo(string plateNo, string surname, string carMake, RepairListMode mode)
        {
            int userId = db.Users.FirstOrDefault(u => u.UserName.Equals(User.Identity.Name)).Id;
            int mechanicId = db.Mechanics.FirstOrDefault(m => m.UserID.Equals(userId)).MechanicID;
            if (mode == RepairListMode.Incoming)
            {
                var incomingRepairs = db.IncomingRepairs.Where(i => i.MechanicId == null || (i.MechanicId == mechanicId && i.Status == 0)).ToList();
                return View("IncomingRepairListTable", incomingRepairs);
            }
            var repairs = db.Repairs.Where(r => r.MechanicID.Equals(mechanicId)).ToList();
            repairs = FilterWithMode(repairs, mode);
            if(plateNo != "")
            {
                repairs = repairs.Where(r => r.ClientXCar.PlateNo.ToUpper().Equals(plateNo.ToUpper())).ToList();
            }
            if(surname != "")
            {
                repairs = repairs.Where(r => r.Client.User.Surname.ToUpper().Equals(surname.ToUpper())).ToList();
            }
            if(carMake != "")
            {
                repairs = repairs.Where(r => r.ClientXCar.Car.Make.ToUpper().Equals(carMake.ToUpper())).ToList();
            }
            return View("RepairListTable", repairs);
        }

        private List<Repair> FilterWithMode(List<Repair> list, RepairListMode mode)
        {
            switch(mode){
                case RepairListMode.Finished:
                    list = list.Where(r => r.IsFinished).ToList();
                    break;
                case RepairListMode.NotFinished:
                    list = list.Where(r => !r.IsFinished).ToList();
                    break;
                case RepairListMode.Incoming:
                    list = list.Where(r => r.StartDate == null).ToList();
                    break;
            }
            return list;
        }


        [HttpGet]
        public ActionResult CreateRepair(DateTime? startDate)
        {
            if (User.Identity.IsAuthenticated)
            {
                int userId = db.Users.FirstOrDefault(u => u.UserName.Equals(User.Identity.Name)).Id;
                int mechanicId = db.Mechanics.FirstOrDefault(m => m.UserID.Equals(userId)).MechanicID;
                int maxId = 0;
                if (db.Repairs.Count() > 0)
                {
                    maxId = db.Repairs.Max(r => r.RepairID);
                }
                CreateRepairModel createRepairModel = new CreateRepairModel();
                Repair repair = new Repair { RepairID = maxId + 1, MechanicID = mechanicId, RequestDate = DateTime.Now };
                repair.RepairHash = String.Format("{0}-{1}", repair.RequestDate.ToString(@"yyyy\/MM\/dd"), repair.RepairID);
                repair.StartDate = startDate.HasValue ? startDate : DateTime.Now;
                repair.PlannedEndDate = startDate.HasValue ? startDate : DateTime.Now;
                createRepairModel.FormRepair = repair;
                Session["repairPartsList"] = new PartsListModel { PartsList = new List<RepairXPart>() };
                return View(createRepairModel);
            }
            return View("Index");
        }

        [HttpPost]
        public ActionResult AddRepair(CreateRepairModel createRepairModel)
        {
            PartsListModel partsListModel = (PartsListModel)Session["repairPartsList"];
            createRepairModel.FormPartList = partsListModel.PartsList;
            if (!ModelState.IsValid)
                return View("CreateRepair", createRepairModel);

            Client client = db.Clients.Where(c => c.UserID.Equals(createRepairModel.FormUser.Id)).FirstOrDefault();
            if(client == null)
            {
                client = new Client { ClientID = db.Clients.Max(c => c.ClientID) + 1, UserID = createRepairModel.FormUser.Id };
                client = db.Clients.Add(client);
                db.SaveChanges();
            }
            createRepairModel.FormUser = db.Users.Find(createRepairModel.FormUser.Id);

            Car car;
            car = db.Cars
                    .Where(c => c.Make.ToUpper().Equals(createRepairModel.FormCar.Make.ToUpper()) 
                    && c.Model.ToUpper().Equals(createRepairModel.FormCar.Model.ToUpper())
                    && c.Year.Equals(createRepairModel.FormCar.Year)
                    && c.Capacity.Equals(createRepairModel.FormCar.Capacity)
                    && c.Bhp.Equals(createRepairModel.FormCar.Bhp)
                    && c.Version.Equals(createRepairModel.FormCar.Version))
                    .FirstOrDefault();
            if (car == null)
            {
                createRepairModel.FormCar.CarID = db.Cars.Max(c => c.CarID) + 1;
                car = db.Cars.Add(createRepairModel.FormCar);
                db.SaveChanges();
            }

            ClientXCar clientXCar = db.ClientsXCars.Where(cxc => cxc.ClientXCarID.Equals(createRepairModel.FormClientXCar.ClientXCarID))
                                                    .FirstOrDefault();

            if(clientXCar == null)
            {
                int maxId = 0;
                if(db.ClientsXCars.Count() > 0)
                {
                    maxId = db.ClientsXCars.Max(c => c.ClientXCarID);
                }
                clientXCar = new ClientXCar {
                    ClientXCarID = maxId + 1,
                    CarID = car.CarID, ClientID = client.ClientID,
                    PlateNo = createRepairModel.FormClientXCar.PlateNo };
                clientXCar = db.ClientsXCars.Add(clientXCar);
                db.SaveChanges();
            }
            createRepairModel.FormClientXCar = clientXCar;
            createRepairModel.FormRepair.ClientID = client.ClientID;
            createRepairModel.FormRepair.ClientXCarID = clientXCar.ClientXCarID;
            createRepairModel.FormRepair.RequestDate = DateTime.Now;
            createRepairModel.FormRepair = db.Repairs.Add(createRepairModel.FormRepair);

            foreach (var repairXPart in createRepairModel.FormPartList)
            {
                Part part = db.Parts.Where(p => p.Name.ToUpper().Equals(repairXPart.Part.Name.ToUpper())
                                    && p.Price.Equals(repairXPart.Part.Price)).FirstOrDefault();

                if (part == null)
                {
                    int partMaxId = 0;
                    if (db.Parts.Count() > 0)
                        partMaxId = db.Parts.Max(p => p.PartID);
                    part = new Part { PartID = partMaxId + 1, Name = repairXPart.Part.Name, Price = repairXPart.Part.Price };
                    part = db.Parts.Add(part);

                    RepairXPart rXp = new RepairXPart
                    {
                        RepairXPartID = repairXPart.RepairXPartID,
                        RepairID = createRepairModel.FormRepair.RepairID,
                        PartID = part.PartID,
                        Quantity = repairXPart.Quantity,
                        Total = part.Price * repairXPart.Quantity
                    };
                    rXp = db.RepairsXParts.Add(rXp);
                    db.SaveChanges();
                }
                else
                {
                    RepairXPart rXp = db.RepairsXParts.Where(r => r.PartID.Equals(part.PartID)
                                                  && r.RepairID.Equals(createRepairModel.FormRepair.RepairID)).FirstOrDefault();
                    if (rXp != null)
                    {
                        rXp.Quantity += repairXPart.Quantity;
                        rXp.Total = part.Price * rXp.Quantity;
                    }
                    else
                    {
                        rXp = new RepairXPart
                        {
                            RepairXPartID = repairXPart.RepairXPartID,
                            RepairID = createRepairModel.FormRepair.RepairID,
                            PartID = part.PartID,
                            Quantity = repairXPart.Quantity,
                            Total = part.Price * repairXPart.Quantity
                        };
                        rXp = db.RepairsXParts.Add(rXp);
                    }
                }
            }
            db.SaveChanges();
            return RedirectToAction("CreateInvoice", new { repairId = createRepairModel.FormRepair.RepairID, returnUrl = "/Mechanic/" });
        }

        public ActionResult CancelCreateRepair(bool isFromRequest, int? incomingRepairId)
        {
            if (isFromRequest && incomingRepairId != null)
            {
                var incomingRepair = db.IncomingRepairs.FirstOrDefault(i => i.Id.Equals(incomingRepairId.Value));
                incomingRepair.Status = 0;
                incomingRepair.MechanicId = null;
                db.SaveChanges();
            }
            Session["repairPartsList"] = null;
            Session["repairPartsToDeleteList"] = null;
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult DeleteRepair(int repairId)
        {
            var repairXParts = db.RepairsXParts.Where(r => r.RepairID.Equals(repairId)).ToList();
            foreach (var repairXPart in repairXParts)
            {
                db.RepairsXParts.Remove(repairXPart);
            }
            var invoice = db.Invoices.FirstOrDefault(i => i.RepairID.Equals(repairId));
            db.Invoices.Remove(invoice);
            var repair = db.Repairs.Where(r => r.RepairID.Equals(repairId)).FirstOrDefault();
            db.Repairs.Remove(repair);
            db.SaveChanges();
            return Json("List");
        }

        [HttpGet]
        public ActionResult EditRepair(int repairId, string returnUrl)
        {
            RepairEditModel repairEdit = new RepairEditModel();
            repairEdit.RepairXParts = new List<RepairXPart>();
            Repair repair = db.Repairs.Where(r => r.RepairID.Equals(repairId)).FirstOrDefault();
            var repairXParts = db.RepairsXParts.Where(r => r.RepairID.Equals(repair.RepairID)).ToList();
            Client client = db.Clients.Where(c => c.ClientID.Equals(repair.ClientID)).FirstOrDefault();
            User user = db.Users.Where(u => u.Id.Equals(client.UserID)).FirstOrDefault();
            ClientXCar clientXCar = db.ClientsXCars.Where(c => c.ClientXCarID.Equals(repair.ClientXCarID)).FirstOrDefault();
            Car car = db.Cars.Where(c => c.CarID.Equals(clientXCar.CarID)).FirstOrDefault();
            repairEdit.Repair = repair;
            repairEdit.UserClient = user;
            repairEdit.ClientXCar = clientXCar;
            repairEdit.Car = car;
            repairEdit.RepairXParts = repairXParts;
            Session["repairPartsList"] = new PartsListModel { PartsList = new List<RepairXPart>() };
            Session["repairPartsToDeleteList"] = new PartsListModel { PartsList = new List<RepairXPart>() };
            Session["editRepairReturnUrl"] = returnUrl;
            return View(repairEdit);
        }

        [HttpPost]
        public ActionResult EditRepair(RepairEditModel repairEditModel)
        {
            Repair repair = db.Repairs.Where(r => r.RepairID.Equals(repairEditModel.Repair.RepairID)).FirstOrDefault();
            repair.StartDate = repairEditModel.Repair.StartDate;
            repair.PlannedEndDate = repairEditModel.Repair.PlannedEndDate;
            repair.IsFinished = repairEditModel.Repair.IsFinished;
            repair.Description = repairEditModel.Repair.Description;
            PartsListModel partsListModel = (PartsListModel)Session["repairPartsList"];
            PartsListModel partsToDeleteModel = (PartsListModel)Session["repairPartsToDeleteList"];
            foreach(var item in partsListModel.PartsList)
            {
                Part part = db.Parts.Where(p => p.Name.ToUpper().Equals(item.Part.Name.ToUpper())
                                    && p.Price.Equals(item.Part.Price)).FirstOrDefault();

                if (part == null)
                {
                    int partMaxId = 0;
                    if (db.Parts.Count() > 0)
                        partMaxId = db.Parts.Max(p => p.PartID);
                    part = new Part { PartID = partMaxId + 1, Name = item.Part.Name, Price = item.Part.Price };
                    part = db.Parts.Add(part);

                    RepairXPart rXp = new RepairXPart
                    {
                        RepairXPartID = item.RepairXPartID,
                        RepairID = repair.RepairID,
                        PartID = part.PartID,
                        Quantity = item.Quantity,
                        Total = part.Price * item.Quantity
                    };
                    rXp = db.RepairsXParts.Add(rXp);
                    db.SaveChanges();
                }
                else
                {
                    RepairXPart rXp = db.RepairsXParts.Where(r => r.PartID.Equals(part.PartID)
                                                  && r.RepairID.Equals(repair.RepairID)).FirstOrDefault();
                    if (rXp != null)
                    {
                        rXp.Quantity += item.Quantity;
                        rXp.Total = part.Price * rXp.Quantity;
                    }
                    else
                    {
                        rXp = new RepairXPart
                        {
                            RepairXPartID = item.RepairXPartID,
                            RepairID = repair.RepairID,
                            PartID = part.PartID,
                            Quantity = item.Quantity,
                            Total = part.Price * item.Quantity
                        };
                        rXp = db.RepairsXParts.Add(rXp);
                    }
                }
            }
            foreach (var item in partsToDeleteModel.PartsList)
            {
                var rxp = db.RepairsXParts.Where(r => r.RepairXPartID.Equals(item.RepairXPartID) 
                                           && r.RepairID.Equals(repair.RepairID)).FirstOrDefault();
                if(rxp != null)
                {
                    db.RepairsXParts.Remove(rxp);
                }
            }
            var invoice = db.Invoices.FirstOrDefault(i => i.RepairID.Equals(repair.RepairID));
            invoice.GrossAmount = repair.RepairXParts.Sum(r => r.Total);
            db.SaveChanges();
            string s = (string)Session["editRepairReturnUrl"];
            return Redirect(s);
        }

        public ActionResult ClaimIncomingRepair(int incomingRepairId)
        {
            int userId = User.Identity.GetUserId<int>();
            int mechanicId = db.Mechanics.FirstOrDefault(m => m.UserID.Equals(userId)).MechanicID;
            var incomingRepair = db.IncomingRepairs.FirstOrDefault(i => i.Id.Equals(incomingRepairId));
            incomingRepair.MechanicId = mechanicId;
            incomingRepair.Status = 1;
            int maxId = 0;
            if (db.Repairs.Count() > 0)
            {
                maxId = db.Repairs.Max(r => r.RepairID);
            }
            CreateRepairModel createRepairModel = new CreateRepairModel();
            Repair repair = new Repair { RepairID = maxId + 1, MechanicID = mechanicId, RequestDate = DateTime.Now };
            repair.RepairHash = String.Format("{0}-{1}", repair.RequestDate.ToString(@"yyyy\/MM\/dd"), repair.RepairID);
            repair.StartDate = DateTime.Now;
            repair.PlannedEndDate = DateTime.Now;
            if(incomingRepair.ClientXCarId != null)
            {
                repair.ClientXCarID = incomingRepair.ClientXCarId.Value;
                repair.ClientID = incomingRepair.ClientId;
                createRepairModel.FormCar = incomingRepair.ClientXCar.Car;
                createRepairModel.FormClientXCar = incomingRepair.ClientXCar;
            }
            db.SaveChanges();
            createRepairModel.FormRepair = repair;
            createRepairModel.FormUser = incomingRepair.Client.User;
            createRepairModel.IsFromRequest = true;
            createRepairModel.FormIncomingRepairID = incomingRepairId;
            Session["repairPartsList"] = new PartsListModel { PartsList = new List<RepairXPart>() };
            return View("CreateRepair", createRepairModel);
        }

        [HttpPost]
        public ActionResult FindPlateNo(string plateNo)
        {
            var clientXCar = db.ClientsXCars.Where(c => c.PlateNo.ToUpper().Equals(plateNo.ToUpper())).FirstOrDefault();
            if (clientXCar != null)
            {
                return Json(new
                {
                    Success = 1,
                    Id = clientXCar.ClientXCarID,
                    PlateNo = clientXCar.PlateNo.ToUpper(),
                    CarMake = clientXCar.Car.Make,
                    CarModel = clientXCar.Car.Model,
                    CarCapacity = clientXCar.Car.Capacity,
                    CarVersion = clientXCar.Car.Version,
                    CarYear = clientXCar.Car.Year,
                    CarBhp = clientXCar.Car.Bhp,
                    UserName = clientXCar.Client.User.Name,
                    UserSurname = clientXCar.Client.User.Surname,
                    UserPhone = clientXCar.Client.User.Phone,
                    UserId = clientXCar.Client.User.Id
                });
            }
            else
            {
                return Json(new { Success = 0 });
            }
        }

        public ActionResult CreateInvoice(int repairId, string returnUrl)
        {
            CreateInvoiceModel createInvoiceModel = new CreateInvoiceModel();
            createInvoiceModel.PartsList = new List<KeyValuePair<RepairXPart, Part>>();

            Repair repair = db.Repairs.Where(r => r.RepairID.Equals(repairId)).FirstOrDefault();
            var repairXParts = db.RepairsXParts.Where(r => r.RepairID.Equals(repair.RepairID)).ToList();
            decimal grossAmount = 0;
            foreach (var rxp in repairXParts)
            {
                Part part = db.Parts.Where(p => p.PartID.Equals(rxp.PartID)).FirstOrDefault();
                createInvoiceModel.PartsList.Add(new KeyValuePair<RepairXPart, Part>(rxp, part));
                grossAmount += rxp.Total;
            }

            Invoice invoice = db.Invoices.Where(i => i.RepairID.Equals(repair.RepairID)).FirstOrDefault();
            if(invoice == null)
            {
                invoice = new Invoice
                {
                    RepairID = repair.RepairID,
                    IssueDate = DateTime.Now,
                    GrossAmount = grossAmount,
                    CurrencyID = 1,
                    IsPaid = false
                };
                invoice = db.Invoices.Add(invoice);
                db.SaveChanges();
            }

            Client client = db.Clients.Where(c => c.ClientID.Equals(repair.ClientID)).FirstOrDefault();
            User user = db.Users.Where(u => u.Id.Equals(client.UserID)).FirstOrDefault();
            ClientXCar clientXCar = db.ClientsXCars.Where(c => c.ClientXCarID.Equals(repair.ClientXCarID)).FirstOrDefault();
            Car car = db.Cars.Where(c => c.CarID.Equals(clientXCar.CarID)).FirstOrDefault();
            
            createInvoiceModel.Repair = repair;
            createInvoiceModel.Invoice = invoice;
            createInvoiceModel.Client = user;
            createInvoiceModel.Car = car;
            createInvoiceModel.ClientXCar = clientXCar;
            Session["createInvoiceReturnUrl"] = returnUrl;
            return View(createInvoiceModel);
        }

        [HttpPost]
        public ActionResult SaveInvoice(Invoice invoice)
        {
            var dbInvoice = db.Invoices.Where(i => i.InvoiceID.Equals(invoice.InvoiceID)).FirstOrDefault();
            dbInvoice.IsPaid = invoice.IsPaid;
            dbInvoice.DueDate = invoice.DueDate;
            dbInvoice.PaymentDay = invoice.PaymentDay;
            db.SaveChanges();
            return Redirect((string)Session["createInvoiceReturnUrl"]);
        }

        [HttpPost]
        public ActionResult Find(string surname)
        {
            int clientRoleID = db.Roles.First(r => r.Name.Equals("Client")).Id;
            int guestRoleID = db.Roles.First(r => r.Name.Equals("GuestClient")).Id;
            var clients = db.Users.Where(u => u.Surname.Contains(surname) && (u.RoleID == clientRoleID || u.RoleID == guestRoleID))
                                  .OrderBy(u => u.Surname)
                                  .Select(s => new { Id = s.Id, Surname = s.Surname, Name = s.Name, Phone = s.Phone })
                                  .ToList()
                                  .Select(s => new User { Id = s.Id, Name = s.Name, Surname = s.Surname, Phone = s.Phone});
            return PartialView("ShortClientsList", clients);
        }

        [HttpGet]
        public ActionResult AddTemporaryClient(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        public ActionResult AddTemporaryClient(User user, string returnUrl)
        {
            UserManager<User, int> userManager = Startup.UserManagerFactory.Invoke();
            user.RoleID = db.Roles.First(r => r.Name.Equals("GuestClient")).Id;
            user.UserName = user.Email;
            var result = userManager.Create(user);
            if (result.Succeeded)
            {
                Client client = new Client();
                client.UserID = user.Id;
                db.Clients.Add(client);
                db.SaveChanges();
                return Redirect(returnUrl);
            }

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
            return View();
        }

        public ActionResult ClientsList()
        {
            int userID = User.Identity.GetUserId<int>();
            int mechanicID = db.Mechanics.Where(m => m.UserID.Equals(userID)).FirstOrDefault().MechanicID;
            var clients = db.Repairs.Where(r => r.MechanicID.Equals(mechanicID)).Select(r => r.Client).Distinct().OrderBy(r => r.User.Surname);
            return View(clients);
        }

        public ActionResult AddPartToList(string partName, decimal partPrice, int quantity)
        {
            var part = db.Parts.Where(p => p.Name.Equals(partName) && p.Price.Equals(partPrice)).FirstOrDefault();
            if(part == null)
            {
                part = new Part { Name = partName, Price = partPrice };
                
            }
            PartsListModel partsList = (PartsListModel)Session["repairPartsList"];
            if(partsList == null)
            {
                partsList = new PartsListModel();
                Session["repairPartsList"] = partsList;
            }
            int rxpMaxId = db.RepairsXParts.Max(r => r.RepairXPartID);
            RepairXPart rxp = new RepairXPart { RepairXPartID = rxpMaxId + 1, Part = part, Quantity = quantity, Total = (decimal)(part.Price * quantity) };
            partsList.PartsList.Add(rxp);
            return PartialView("PartRow", rxp);
        }

        public int DeletePartFromList(int repairXPartId)
        {
            PartsListModel partsList = (PartsListModel)Session["repairPartsList"];
            PartsListModel partsToDelete = (PartsListModel)Session["repairPartsToDeleteList"];
            var toDelete = partsList.PartsList.First(r => r.RepairXPartID.Equals(repairXPartId));
            partsList.PartsList.Remove(toDelete);
            partsToDelete.PartsList.Add(toDelete);
            return repairXPartId;
        }

        public ActionResult CarMakeList()
        {
            var carMakes = db.Cars.Select(s => s.Make.ToString()).Distinct().ToList();
            return View("DropdownCarDescList", carMakes);
        }

        public ActionResult CarModelList(string make)
        {
            var carModels = db.Cars.Where(c => c.Make.Equals(make)).Select(s => s.Model.ToString()).Distinct().ToList();
            return View("DropdownCarDescList", carModels);
        }

        public ActionResult CarVersionList(string make, string model)
        {
            var carVersions = db.Cars.Where(c => c.Make.Equals(make) && c.Model.Equals(model)).Select(s => s.Version.ToString()).Distinct().ToList();
            return View("DropdownCarDescList", carVersions);
        }

        [HttpGet]
        public ActionResult AddCar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddCar([Bind(Exclude = "CarID")] Car car)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var dbCar = db.Cars
                    .Where(c => c.Make.ToUpper().Equals(car.Make.ToUpper())
                    && c.Model.ToUpper().Equals(car.Model.ToUpper())
                    && c.Year.Equals(car.Year)
                    && c.Capacity.Equals(car.Capacity)
                    && c.Bhp.Equals(car.Bhp)
                    && c.Version.ToUpper().Equals(car.Version.ToUpper()))
                    .FirstOrDefault();

            if(dbCar != null)
            {
                ModelState.AddModelError("", "Car is already in database");
                return View();
            }
            else
            {
                db.Cars.Add(car);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }
    }
}