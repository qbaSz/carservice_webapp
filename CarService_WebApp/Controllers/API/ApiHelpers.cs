﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CarService_WebApp.Db;
using CarService_WebApp.Models;

namespace CarService_WebApp.Controllers.API
{
    public static class ApiHelpers
    {
        public static Mechanic GetMechanicFromDbById(EFDbContext context, int id)
        {
            return context.Mechanics.SingleOrDefault(m => m.MechanicID == id);
        }

        public static Client GetClientFromDbById(EFDbContext context, int id)
        {
            return context.Clients.SingleOrDefault(m => m.ClientID == id);
        }

        public static ClientXCar GetClientXCarFromDbById(EFDbContext context, int id)
        {
            return context.ClientsXCars.SingleOrDefault(m => m.ClientXCarID == id);
        }

        public static Car GetCarFromDbByIdOfClientXCar(EFDbContext context, int id)
        {
            var clientXCar = context.ClientsXCars.SingleOrDefault(cxc => cxc.ClientXCarID == id);
            return context.Cars.SingleOrDefault(m => m.CarID == clientXCar.CarID);
        }

        public static bool IsDateInRange(DateTime dateToCheck, DateTime startDate, DateTime endDate)
        {
            return dateToCheck >= startDate && dateToCheck <= endDate;
        }

        internal enum RepairStatus
        {
            New = 1
        }

        internal enum Roles
        {
            Client = 1,
            Mechanic = 2,
            Admin = 3,
            GuestClient = 4
        }
    }
}