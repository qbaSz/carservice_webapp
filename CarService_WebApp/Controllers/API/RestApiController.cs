﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using CarService_WebApp.Db;
using CarService_WebApp.DTOs;
using CarService_WebApp.Models;
using NLog;
using static CarService_WebApp.Controllers.API.ApiHelpers;

namespace CarService_WebApp.Controllers.API
{
    [Authorize] // - OAuth 2
    //[IdentityBasicAuthentication] //Enable authentication via an ASP.NET Identity user name and password - Basic authentication
    [RoutePrefix("api/rest")]
    public class RestApiController : ApiController
    {
        private readonly EFDbContext _context;
        public static Logger Logger;

        public RestApiController()
        {
            _context = new EFDbContext();
            Logger = LogManager.GetCurrentClassLogger();
        }

        #region Parts
        [HttpGet]
        [Route("parts")]
        public IEnumerable<PartDto> GetParts()
        {
            return 
                _context.Parts
                .ToList()
                .Select(Mapper.Map<Part, PartDto>);
        }

        [HttpGet]
        [Route("parts/{id}")]
        public IHttpActionResult GetPart(int id)
        {
            var part = _context.Parts.SingleOrDefault(p => p.PartID == id);

            if (part == null)
                return NotFound();
            
            return Ok(Mapper.Map<Part,PartDto>(part));
        }
        #endregion

        #region Repairs
        [HttpGet]
        [Route("repairs")]
        public HttpResponseMessage GetRepairs()
        {
            var repairsDto = 
                _context.Repairs
                .Select(Mapper.Map<Repair, RepairDto>)
                .ToList();

            return Request.CreateResponse(HttpStatusCode.OK, new {count = repairsDto.Count , repairs = repairsDto});
        }

        [HttpGet]
        [Route("repairs/{id}")]
        public IHttpActionResult GetRepair(int id)
        {
            var repairDb = _context.Repairs.SingleOrDefault(r => r.RepairID == id);

            if (repairDb == null)
                return NotFound();

            var repairDto = Mapper.Map<Repair, RepairDto>(repairDb);

            return Ok(repairDto);
        }

        [HttpGet]
        [Route("repairs")]
        public HttpResponseMessage GetRepairsOfUserWithId(int userId)
        {
            var client = _context.Clients.SingleOrDefault(c => c.UserID.Equals(userId));
            if (client == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);

            var repairsDto = 
                _context.Repairs
                .Where(r => r.ClientID == client.ClientID)
                .Select(Mapper.Map<Repair, RepairDto>)
                .ToList();

            return
                Request.CreateResponse(HttpStatusCode.OK, new { count = repairsDto.Count, repairs = repairsDto });
        }
        #endregion

        #region Users
        [HttpGet]
        [Route("clients")]
        public HttpResponseMessage GetClients()
        {
            var clientsDto =
                _context.Clients
                .Select(Mapper.Map<Client, ClientDto>)
                .ToList();

            return
                Request.CreateResponse(HttpStatusCode.OK, new { count = clientsDto.Count, clients = clientsDto });
        }

        [HttpGet]
        [Route("clients/")]
        public IHttpActionResult GetClient(string clientLogin)
        {
            var clientInDb = _context.Clients.SingleOrDefault(c => c.User.Email == clientLogin);

            return Ok(Mapper.Map<Client, ClientDto>(clientInDb));
        }

        [HttpGet]
        [Route("users/{id}")]
        public IHttpActionResult GetUser(int id)
        {
            var userInDb = _context.Users.SingleOrDefault(u => u.Id == id);

            return Ok(Mapper.Map<User, UserDto>(userInDb));
        }

        [HttpGet]
        [Route("users")]
        public IHttpActionResult GetUserByLogin(string userLogin)
        {
            var userInDb = _context.Users.SingleOrDefault(u => u.Email == userLogin);
            var userDto = Mapper.Map<User, UserDto>(userInDb);

            if (userDto.Role.Id == (byte) Roles.Mechanic)
            {
                userDto.MechanicId = _context.Mechanics.SingleOrDefault(m => m.User.Id == userDto.Id)?.MechanicID;
            }

            return Ok(userDto);
        }

        [HttpPut]
        [Route("users/{id}")]
        public IHttpActionResult UpdateUserBasicData(int id, string n, string sn)
        {
            var userInDb = _context.Users.SingleOrDefault(u => u.Id == id);

            if (userInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            var userDto = Mapper.Map<User, UserDto>(userInDb);
            userDto.Name = n;
            userDto.Surname = sn;

            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    Mapper.Map(userDto, userInDb);

                    _context.SaveChanges();

                    dbContextTransaction.Commit();
                }
                catch (Exception)
                {
                    dbContextTransaction.Rollback();
                    throw new HttpResponseException(HttpStatusCode.ExpectationFailed);
                }
            }

            userInDb = _context.Users.SingleOrDefault(u => u.Id == id);

            return Ok(Mapper.Map<User, UserDto>(userInDb));
        }

        [HttpPut]
        [Route("users/{id}")]
        public IHttpActionResult UpdateUserObjectData(int id, UserDto userDto)  //TODO: to fix
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var userInDb = _context.Users.SingleOrDefault(u => u.Id == id);

            if (userInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            Mapper.Map(userDto, userInDb);

            _context.SaveChanges();

            userInDb = _context.Users.SingleOrDefault(u => u.Id == id);

            return Ok(Mapper.Map<User, UserDto>(userInDb));
        }

        [HttpGet]
        [Route("mechanics")]
        public IHttpActionResult GetMechanics()
        {
            var mechanicsDto =
                _context.Mechanics
                .Select(Mapper.Map<Mechanic, MechanicDto>)
                .ToList();

            return Ok(new {count = mechanicsDto.Count, mechanics = mechanicsDto});
        }

        #endregion

        #region Repair requests
        [HttpGet]
        [Route("incomingrepairs")]
        public IEnumerable<IncomingRepairDto> GetIncomingRepairs()
        {
            return
                _context.IncomingRepairs
                .Select(Mapper.Map<IncomingRepair, IncomingRepairDto>)
                .ToList();
        }

        [HttpGet]
        [Route("incomingrepairs")]
        public IHttpActionResult GetIncomingRepairsOfMechanic(int mechanicId)
        {
            var incomingRepairsDtos =_context.IncomingRepairs
                    .Where(ir => ir.Mechanic.MechanicID == mechanicId)
                    .Select(Mapper.Map<IncomingRepair, IncomingRepairDto>)
                    .ToList();

            return Ok(new { count = incomingRepairsDtos.Count, incomingRepairs = incomingRepairsDtos });
        }

        [HttpGet]
        [Route("incomingrepairs/{id}")]
        public IHttpActionResult GetIncomingRepair(int id)
        {
            var incomingRepair = _context.IncomingRepairs.SingleOrDefault(ir => ir.Id == id);

            if (incomingRepair == null)
                return NotFound();

            return
                Ok(Mapper.Map<IncomingRepair, IncomingRepairDto>(incomingRepair));
        }

        [HttpPost]
        [Route("incomingrepairs")]
        public IHttpActionResult CreateNewIncomingRepair(IncomingRepairDto incomingRepairDto, int userId)
        {
            var client = _context.Clients.SingleOrDefault(c => c.UserID.Equals(userId));
            if (client == null)
                return NotFound();

            incomingRepairDto.IncomingRepairDateTime = DateTime.Now;
            incomingRepairDto.Status = (byte) RepairStatus.New;
            incomingRepairDto.ClientId = client.ClientID;

            if (!_context.Mechanics.Any(m => m.MechanicID == incomingRepairDto.MechanicId)
                || !_context.ClientsXCars.Any(m => m.ClientXCarID == incomingRepairDto.ClientXCarId))
            {
                return Content(HttpStatusCode.Conflict, 
                    new { message = "No mechanics or cars found with given Ids." });
            }

            var theSameRepairRequestInDb = 
                _context.IncomingRepairs
                .OrderByDescending(i => i.IncomingRepairDateTime)
                .FirstOrDefault(i => i.ClientXCarId == incomingRepairDto.ClientXCarId);

            if (theSameRepairRequestInDb != null
                && IsDateInRange(theSameRepairRequestInDb.IncomingRepairDateTime, DateTime.Now.AddDays(-1), DateTime.Now))
            {
                return Content(HttpStatusCode.Conflict,
                    new { message = "You cannot add more than one repair request of that car within 24 hours" });
            }

            var incomingRepair = Mapper.Map<IncomingRepairDto, IncomingRepair>(incomingRepairDto);

            _context.IncomingRepairs.Add(incomingRepair);
            _context.SaveChanges();

            incomingRepairDto.Id = incomingRepair.Id;

            return
                Created(new Uri(Request.RequestUri + "/" + incomingRepair.Id), incomingRepairDto);
        }
        #endregion

        #region ClientXCars
        [HttpGet]
        [Route("clientxcars")]
        public IHttpActionResult GetClientXCarsByUserId(int userId)
        {
            var clientXCarDtos = _context
                .ClientsXCars
                .Where(cxc => cxc.Client.User.Id.Equals(userId))
                .Select(Mapper.Map<ClientXCar, ClientXCarDto>)
                .ToList();

            if (!clientXCarDtos.Any())
                return NotFound();

            return
                Ok(new { count = clientXCarDtos.Count, clientXCars = clientXCarDtos });
        }

        [HttpGet]
        [Route("clientxcars/{id}")]
        public IHttpActionResult GetClientXCar(int id)
        {
            var clientXCar = _context
                .ClientsXCars
                .FirstOrDefault(cxc => cxc.ClientXCarID.Equals(id));

            if (clientXCar == null)
                return NotFound();

            var clientXCarDto = Mapper.Map<ClientXCar, ClientXCarDto>(clientXCar);

            return
                Ok(clientXCarDto);
        }

        [HttpPost]
        [Route("clientxcars")]
        public IHttpActionResult AddCar(CarDto carDto, int userId, string plateNo)
        {
            var client = _context.Clients.SingleOrDefault(c => c.UserID.Equals(userId));

            if (client == null)
                return NotFound();

            var existsCarWithTheSamePlateNo = _context.ClientsXCars.Any(c => c.PlateNo.Equals(plateNo)); 

            if (existsCarWithTheSamePlateNo)
                return Content(HttpStatusCode.Conflict,
                    new { message = "There already exists car with given plate number" });

            var car = Mapper.Map<CarDto, Car>(carDto);

            _context.Cars.Add(car);
            _context.SaveChanges();
            
            var clientXCar = new ClientXCar()
            {
                CarID = car.CarID,
                ClientID = client.ClientID,
                PlateNo = plateNo,
                IsActive = true
            };
            
            _context.ClientsXCars.Add(clientXCar);
            _context.SaveChanges();

            return
                Ok(new {clientXCar.ClientXCarID});
        }

        [HttpPut]
        [Route("clientxcars/{id}")]
        public IHttpActionResult SetClientsCarFlags(int id, bool? isDefault, bool? isActive)
        {
            var clientsCarDb = _context.ClientsXCars.SingleOrDefault(cxc => cxc.ClientXCarID == id);

            if (clientsCarDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            var clientsCarDto = Mapper.Map<ClientXCar, ClientXCarDto>(clientsCarDb);

            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    if (isDefault != null)
                    {
                        if (isDefault == false && clientsCarDb.IsDefault || isDefault == true)
                        {
                            var clientsCars =
                                _context.ClientsXCars.Where(cxc => cxc.ClientID == clientsCarDb.ClientID);
                            foreach (var cxc in clientsCars)
                            {
                                cxc.IsDefault = false;
                            }
                        }
                        clientsCarDto.IsDefault = (bool)isDefault;

                        _context.SaveChanges();
                    }
                    if (isActive != null)
                    {
                        clientsCarDto.IsActive = (bool)isActive;
                    }
                    Mapper.Map(clientsCarDto, clientsCarDb);

                    _context.SaveChanges();

                    dbContextTransaction.Commit();
                }
                catch (Exception)
                {
                    dbContextTransaction.Rollback();
                    throw new HttpResponseException(HttpStatusCode.ExpectationFailed);
                }
            }
            return Ok(clientsCarDto);
        }
        #endregion

        #region Invoices
        [HttpGet]
        [Route("invoices")]
        public IHttpActionResult GetInvoicesByMechanicUserWithId(int userId)
        {
            var client = _context.Clients.SingleOrDefault(c => c.UserID.Equals(userId));
            if (client == null)
                return NotFound();

            var invoicesDtos = _context
                .Invoices
                .Where(inv => inv.Repair.Mechanic.User.Id.Equals(userId))
                .Select(Mapper.Map<Invoice, InvoiceDto>)
                .ToList();

            if (!invoicesDtos.Any())
                return NotFound();

            return
                Ok(new { count = invoicesDtos.Count, invoices = invoicesDtos });
        }
        #endregion

        #region Others
        [HttpGet]
        [Route("repairstatus")]
        [AllowAnonymous]
        public IHttpActionResult GetRepairStatusWithoutLogin(string plateNo, string repairHash)
        {
            var repair = _context.Repairs.FirstOrDefault(r => r.ClientXCar.PlateNo.ToLower().Equals(plateNo.ToLower())
                                                              && r.RepairHash == repairHash);
            if (repair == null)
                return NotFound();

            var repairOfflineStatus = Mapper.Map<Repair, RepairOfflineStatusDto>(repair);
            repairOfflineStatus.Price =
                _context.RepairsXParts
                    .Where(r => r.RepairID.Equals(repair.RepairID))
                    .ToList().Sum(r => r.Total);

            return
                Ok(repairOfflineStatus);
        }
        #endregion
    }
}