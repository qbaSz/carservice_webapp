﻿using CarService_WebApp.Db;
using CarService_WebApp.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using System.Web.Http;
using CarService_WebApp.Models.Auth;
using Microsoft.Owin.Security.OAuth;

namespace CarService_WebApp
{
    public class Startup
    {
        public static Func<UserManager<User, int>> UserManagerFactory { get; private set; }
        public static OAuthBearerAuthenticationOptions OAuthBearerOptions { get; private set; }
        public void Configuration(IAppBuilder app)
        {
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Auth/LogIn"),
                SlidingExpiration = true,
                ExpireTimeSpan = TimeSpan.FromHours(2),
            });

            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
            OAuthBearerOptions = new OAuthBearerAuthenticationOptions();

            app.UseOAuthAuthorizationServer(new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/Oauth/Token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromHours(4),
                Provider = new OAuthServerProvider()
            });

            app.UseOAuthBearerAuthentication(OAuthBearerOptions);

            UserManagerFactory = () =>
            {
                var usermanager = new UserManager<User, int>(new UserStore<User, Role, int, UserLogin, UserRole, UserClaim>(new EFDbContext()));
                usermanager.UserValidator = new UserValidator<User, int>(usermanager)
                {
                    AllowOnlyAlphanumericUserNames = false
                };

                return usermanager;
            };

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);
        }
    }
}