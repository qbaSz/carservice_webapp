﻿using System.Security.Claims;
using System.Threading.Tasks;
using CarService_WebApp.Models;
using Microsoft.Owin.Security.OAuth;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;

namespace CarService_WebApp
{
    public class OAuthServerProvider : OAuthAuthorizationServerProvider // https://msdn.microsoft.com/en-us/library/microsoft.owin.security.oauth.oauthauthorizationserverprovider_methods(v=vs.111).aspx
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            UserManager<User, int> userManager = Startup.UserManagerFactory.Invoke();

            var identityUser = await userManager.FindAsync(context.UserName, context.Password);

            if (identityUser != null)
            {
                try
                {
                    ClaimsIdentity identity = await userManager.CreateIdentityAsync(identityUser, DefaultAuthenticationTypes.ExternalBearer);
                    identity.AddClaim(new Claim(ClaimTypes.GivenName, identityUser.Name));
                    identity.AddClaim(new Claim(ClaimTypes.Surname, identityUser.Surname));
                    identity.AddClaim(new Claim(ClaimTypes.Role, identityUser.Role.Name));
                    context.Validated(identity);
                }
                catch
                {
                    context.SetError("server_error");
                    context.Rejected();
                }
            }
            else
            {
                context.SetError("not_granted", "Incorrect username or password");
                context.Rejected();
            }
        }
    }
}