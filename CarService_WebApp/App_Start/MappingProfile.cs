﻿using AutoMapper;
using CarService_WebApp.DTOs;
using CarService_WebApp.Models;

namespace CarService_WebApp
{
    public class MappingProfile: Profile    
    {
        public MappingProfile() : this("MappingProfile")
        { }
        protected MappingProfile(string profileName) : base(profileName)
        {
            //from Domain to Dto
            CreateMap<Part, PartDto>();
            CreateMap<Repair, RepairDto>();
            CreateMap<Repair, RepairOfflineStatusDto>();
            CreateMap<Mechanic, MechanicDto>();
            CreateMap<Client, ClientDto>();
            CreateMap<ClientXCar, ClientXCarDto>();
            CreateMap<Car, CarDto>();
            CreateMap<IncomingRepair, IncomingRepairDto>();
            CreateMap<User, UserDto>();
            CreateMap<Invoice, InvoiceDto>();
            CreateMap<Currency, CurrencyDto>();

            //from Dto to Domain
            CreateMap<PartDto, Part>();
            CreateMap<RepairDto, Repair>();
            CreateMap<MechanicDto, Mechanic>();
            CreateMap<UserDto, User>();
            CreateMap<ClientDto, Client>();
            CreateMap<ClientXCarDto, ClientXCar>();
            CreateMap<CarDto, Car>().ForMember(c => c.CarID, opt => opt.Ignore()); ;
            CreateMap<IncomingRepairDto, IncomingRepair>();
            CreateMap<InvoiceDto, Invoice>();
        }
    }
}