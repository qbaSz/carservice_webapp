﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarService_WebApp.Models
{
    public class Currency
    {
        public int CurrencyID { get; set; }

        public string Code { get; set; }

        public virtual ICollection<Invoice> Invoices { get; set; }
    }
}