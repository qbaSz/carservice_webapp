﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CarService_WebApp.Models.Home
{
    public class CheckStatusModel
    {
        [Required(ErrorMessage = "Plate number must be filled")]
        [Display(Name = "Plate number")]
        public string PlateNo { get; set; }

        [Required(ErrorMessage = "Repair id must be filled")]
        [Display(Name = "Repair id")]
        public string RepairHash { get; set; }
    }
}