﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarService_WebApp.Models.Home
{
    public class RepairStatusModel
    {
        public string RepairHash { get; set; }
        public string MechanicName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Description { get; set; }
        public bool IsFinished { get; set; }
        public decimal Total { get; set; }
    }
}