﻿namespace CarService_WebApp.Models
{
    public class RepairXPart
    {
        public int RepairXPartID { get; set; }
        public int RepairID { get; set; }
        public int PartID { get; set; }
        public int Quantity { get; set; }
        public decimal Total { get; set; }

        public virtual Repair Repair { get; set; }
        public virtual Part Part { get; set; }
    }
}