﻿using System.Collections.Generic;

namespace CarService_WebApp.Models
{
    public class Client
    {
        public int ClientID { get; set; }
        public int UserID { get; set; }
        public bool HasDiscount { get; set; }
        public int Discount { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<ClientXCar> ClientsCars { get; set; }
        public virtual ICollection<Repair> Repairs { get; set; }
    }
} 