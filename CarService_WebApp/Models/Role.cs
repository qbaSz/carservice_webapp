﻿using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;
using CarService_WebApp.Models.Auth;

namespace CarService_WebApp.Models
{
    public class Role : IdentityRole<int, UserRole> { }
}