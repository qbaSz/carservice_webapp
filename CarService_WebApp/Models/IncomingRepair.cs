﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarService_WebApp.Models
{
    public class IncomingRepair
    {
        public int Id { get; set; }
        [Required]
        public byte Status { get; set; }
        public virtual Client Client { get; set; }
        public int ClientId { get; set; }
        public virtual Mechanic Mechanic { get; set; }
        public int? MechanicId { get; set; }
        public virtual ClientXCar ClientXCar { get; set; }
        public int? ClientXCarId { get; set; }
        public DateTime IncomingRepairDateTime { get; set; }
        [StringLength(300)]
        public string MessageFromClient { get; set; }
    }
}