﻿using System.Collections.Generic;

namespace CarService_WebApp.Models
{
    public class Mechanic
    {
        public int MechanicID { get; set; }
        public int UserID { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<Repair> Repairs { get; set; }
    }
}