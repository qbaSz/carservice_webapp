﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarService_WebApp.Models.Mech
{
    public class CreateInvoiceModel
    {
        public Invoice Invoice { get; set; }
        public Repair Repair { get; set; }
        public User Client { get; set; }
        public List<KeyValuePair<RepairXPart, Part>> PartsList { get; set; }
        public Car Car { get; set; }
        public ClientXCar ClientXCar { get; set; }
    }
}