﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarService_WebApp.Models.Mech
{
    public class PartsListModel
    {
        public List<RepairXPart> PartsList { get; set; }

        public PartsListModel()
        {
            PartsList = new List<RepairXPart>();
        }
    }
}