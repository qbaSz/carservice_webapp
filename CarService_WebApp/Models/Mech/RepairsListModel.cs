﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarService_WebApp.Models.Mech
{
    public class RepairsListModel
    {
        public Repair FormRepair { get; set; }
        public User FormUser { get; set; }
        public ClientXCar FormClientXCar { get; set; }
        public decimal Total { get; set; }
    }
}