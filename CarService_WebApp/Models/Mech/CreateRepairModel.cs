﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarService_WebApp.Models.Mech
{
    public class CreateRepairModel
    {
        public CreateRepairModel()
        {
            IsFromRequest = false;
        }
        public Repair FormRepair { get; set; }
        public Car FormCar { get; set; }
        public User FormUser { get; set; }
        public ClientXCar FormClientXCar { get; set; }
        public IEnumerable<RepairXPart> FormPartList { get; set; }
        public bool IsFromRequest { get; set; }
        public int FormIncomingRepairID { get; set; }
    }
}