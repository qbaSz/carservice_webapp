﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarService_WebApp.Models.Mech
{
    public class RepairEditModel
    {
        public Repair Repair { get; set; }
        public User UserClient { get; set; }
        public ClientXCar ClientXCar { get; set; }
        public Car Car { get; set; }
        public List<RepairXPart> RepairXParts { get; set; }
    }
}