﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using CarService_WebApp.Models.Auth;

namespace CarService_WebApp.Models
{
    public class User : IdentityUser<int, UserLogin, UserRole, UserClaim>
    {
        public User()
        {
            IsAuthenticated = false;
        }
        public int RoleID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public override string Email { get; set; }
        public int Phone { get; set; }
        public string Adress { get; set; }
        public string City { get; set; }
        public bool IsAuthenticated { get; set; }
        public virtual Role Role { get; set; }
    }
}