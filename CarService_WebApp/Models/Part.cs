﻿using System.Collections.Generic;

namespace CarService_WebApp.Models
{
    public class Part
    {
        public int PartID { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }

        public virtual ICollection<RepairXPart> RepairXParts { get; set; }
    }
}