﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarService_WebApp.Models
{
    public class Car
    {
        public int CarID { get; set; }

        [Required(ErrorMessage = "Specify car model")]
        public string Model { get; set; }

        [Required(ErrorMessage = "Specify car make")]
        public string Make { get; set; }

        [Required(ErrorMessage = "Specify car production year")]
        public int Year { get; set; }

        [Required(ErrorMessage = "Specify the capacity od the car(in decimal form)")]
        public decimal Capacity { get; set; }

        [Required(ErrorMessage = "Specify the brake horse power")]
        public int Bhp { get; set; }

        public string Version { get; set; }

        [Display(Name = "VIN Number")]
        public string VinNo { get; set; }
    
        public virtual ICollection<ClientXCar> ClientsCars { get; set; }
    }
}