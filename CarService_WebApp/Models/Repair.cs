﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarService_WebApp.Models
{
    public class Repair
    {
        public int RepairID { get; set; }

        public string RepairHash { get; set; }

        [Column(TypeName = "DateTime2")]
        [DataType(DataType.DateTime)]
        [Display(Name = "Start date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate { get; set; }

        [DataType(DataType.Date)]
        [Column(TypeName = "DateTime2")]
        [Display(Name = "Request date")]
        public DateTime RequestDate { get; set; }

        [DataType(DataType.DateTime)]
        [Column(TypeName = "DateTime2")]
        [Display(Name = "Planned end date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? PlannedEndDate { get; set; }

        [Display(Name = "Repair finished")]
        public bool IsFinished { get; set; }

        public string Description { get; set; }
        public int MechanicID { get; set; }
        public int ClientID { get; set; }
        public int ClientXCarID { get; set; }

        public virtual Mechanic Mechanic { get; set; }
        public virtual Client Client { get; set; }
        public virtual ClientXCar ClientXCar { get; set; }
        public virtual ICollection<RepairXPart> RepairXParts { get; set; }
    }
}