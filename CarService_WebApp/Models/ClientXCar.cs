﻿using System.ComponentModel.DataAnnotations;

namespace CarService_WebApp.Models
{
    public class ClientXCar
    {
        public int ClientXCarID { get; set; }

        [Display(Name = "Plate number")]
        [Required(ErrorMessage = "Specify car plate numbers")]
        public string PlateNo { get; set; }

        public int CarID { get; set; }

        public int ClientID { get; set; }

        public virtual Car Car { get; set; }

        public virtual Client Client { get; set; }

        public bool IsActive { get; set; }

        public bool IsDefault { get; set; }
    }
}