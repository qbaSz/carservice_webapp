﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CarService_WebApp.Models.Auth
{
    public class LogInModel
    {
        [Required(ErrorMessage = "Enter your email")]
        [RegularExpression("^.+\\@.+\\.[a-z]{0,5}$", ErrorMessage = "Email format is illegal")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Enter your password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [HiddenInput]
        public string ReturnUrl { get; set; }
    }
}