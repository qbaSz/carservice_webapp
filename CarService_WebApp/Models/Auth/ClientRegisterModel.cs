﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CarService_WebApp.Models.Auth
{
    public class ClientRegisterModel
    {
        [Required(ErrorMessage = "Enter name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Enter surname")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Enter email")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression("^.+\\@.+\\.[a-z]{0,5}$", ErrorMessage = "Email format is illegal")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Enter phone number")]
        [DataType(DataType.PhoneNumber)]
        public int Phone { get; set; }

        public string Adress { get; set; }
        public string City { get; set; }

        [Required(ErrorMessage = "Provide password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm password")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "Passwords do not match")]
        public string ConfirmPassword { get; set; }
    }
}