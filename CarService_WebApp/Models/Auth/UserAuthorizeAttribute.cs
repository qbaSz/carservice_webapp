﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarService_WebApp.Db;

namespace CarService_WebApp.Models.Auth
{
    public class UserAuthorizeAttribute : AuthorizeAttribute
    {
        public string UserRole { get; set; }
        EFDbContext db = new EFDbContext();
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool isAuthorized =  base.AuthorizeCore(httpContext);
            if (!isAuthorized)
                return false;

            if (!HttpContext.Current.User.Identity.IsAuthenticated && HttpContext.Current.User == null)
                return false; 

            string CurrentUser = HttpContext.Current.User.Identity.Name;
            var CurrentUserRole = db.Users.FirstOrDefault(u => u.UserName == CurrentUser).Role;
            if(UserRole.Contains(CurrentUserRole.Name))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}