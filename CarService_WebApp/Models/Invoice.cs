﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CarService_WebApp.Models
{
    public class Invoice
    {
        public int InvoiceID { get; set; }
        public int RepairID { get; set; }
        public string Number { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime IssueDate { get; set; }

        [Column(TypeName = "DateTime2")]
        [DataType(DataType.Date)]
        [Display(Name = "Due date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DueDate { get; set; }
        public decimal GrossAmount { get; set; }

        [Display(Name = "Is invoice paid")]
        public bool IsPaid { get; set; }
        public decimal NetAmount { get; set; }
        public decimal VatAmount { get; set; }
        public int CurrencyID { get; set; }

        [Column(TypeName = "DateTime2")]
        [DataType(DataType.Date)]
        [Display(Name = "Payment day")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime PaymentDay { get; set; }

        public virtual Currency Currency { get; set; }
        public virtual Repair Repair { get; set; }
    }
}