﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CarService_WebApp.Models;

namespace CarService_WebApp.DTOs
{
    public class CarDto
    {
        public int CarID { get; set; }

        public string Model { get; set; }

        public string Make { get; set; }

        public int Year { get; set; }

        public decimal Capacity { get; set; }

        public int Bhp { get; set; }

        public string Version { get; set; }

        public string VinNo { get; set; }
    }
}