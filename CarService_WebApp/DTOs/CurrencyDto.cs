﻿using System.Collections.Generic;
using CarService_WebApp.Models;

namespace CarService_WebApp.DTOs
{
    public class CurrencyDto
    {
        public int CurrencyID { get; set; }
        public string Code { get; set; }
    }
}