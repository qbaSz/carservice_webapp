﻿using CarService_WebApp.Models;

namespace CarService_WebApp.DTOs
{
    public class UserDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public int Phone { get; set; }
        public string Adress { get; set; }
        public string City { get; set; }
        public Role Role { get; set; }
        public int? MechanicId { get; set; }
    }
}