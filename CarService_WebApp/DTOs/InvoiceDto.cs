﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CarService_WebApp.Models;

namespace CarService_WebApp.DTOs
{
    public class InvoiceDto
    {
        public int InvoiceID { get; set; }

        public int RepairID { get; set; }

        public string Number { get; set; }

        public DateTime IssueDate { get; set; }

        public DateTime DueDate { get; set; }

        public decimal GrossAmount { get; set; }

        public bool IsPaid { get; set; }

        public decimal NetAmount { get; set; }

        public decimal VatAmount { get; set; }

        public DateTime PaymentDay { get; set; }

        public CurrencyDto Currency { get; set; }
    }
}