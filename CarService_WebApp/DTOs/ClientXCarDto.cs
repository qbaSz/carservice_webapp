﻿using System.ComponentModel.DataAnnotations;
using CarService_WebApp.Models;

namespace CarService_WebApp.DTOs
{
    public class ClientXCarDto
    {
        public int ClientXCarID { get; set; }

        public string PlateNo { get; set; }

        public int ClientID { get; set; }

        public virtual CarDto Car { get; set; }

        public bool IsActive { get; set; }

        public bool IsDefault { get; set; }
    }
}