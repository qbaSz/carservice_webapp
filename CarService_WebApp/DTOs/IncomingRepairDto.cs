﻿using System;
using System.ComponentModel.DataAnnotations;
using CarService_WebApp.Models;

namespace CarService_WebApp.DTOs
{
    public class IncomingRepairDto
    {
        public int Id { get; set; }
        [Required]
        public byte Status { get; set; }
        public int ClientId { get; set; }
        public int? MechanicId { get; set; }
        public int? ClientXCarId { get; set; }
        public DateTime IncomingRepairDateTime { get; set; }
        [StringLength(300)]
        public string MessageFromClient { get; set; }
    }
}