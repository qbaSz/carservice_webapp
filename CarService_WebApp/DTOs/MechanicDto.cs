﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CarService_WebApp.Models;

namespace CarService_WebApp.DTOs
{
    public class MechanicDto
    {
        public int MechanicID { get; set; }
        public int UserID { get; set; }

        public UserDto User { get; set; }
    }
}