﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarService_WebApp.DTOs
{
    public class RepairOfflineStatusDto
    {
        public string RepairHash { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? StartDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime RequestDate { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? PlannedEndDate { get; set; }

        public bool IsFinished { get; set; }

        public decimal Price { get; set; }

        public string Description { get; set; }

        public MechanicDto Mechanic { get; set; }

        public ClientXCarDto ClientXCar { get; set; }

    }
}