﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CarService_WebApp.Models;

namespace CarService_WebApp.DTOs
{
    public class PartDto
    {
        public int PartID { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}