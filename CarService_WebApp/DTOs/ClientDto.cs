﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CarService_WebApp.Models;

namespace CarService_WebApp.DTOs
{
    public class ClientDto
    {
        public int ClientID { get; set; }
        public int UserID { get; set; }
        public bool HasDiscount { get; set; }
        public int Discount { get; set; }
        public virtual UserDto User { get; set; }
    }
}