﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarService_WebApp.DTOs
{
    public class RepairDto
    {
        public int RepairID { get; set; }

        public string RepairHash { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime RequestDate { get; set; }

        public DateTime? PlannedEndDate { get; set; }

        public bool IsFinished { get; set; }

        public string Description { get; set; }

        public int MechanicID { get; set; }

        public int ClientID { get; set; }

        public int ClientXCarID { get; set; }

        public MechanicDto Mechanic { get; set; } 

        public ClientDto Client { get; set; } 

        public ClientXCarDto ClientXCar { get; set; }
    }
}